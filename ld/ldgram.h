/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INT = 258,
     NAME = 259,
     LNAME = 260,
     OREQ = 261,
     ANDEQ = 262,
     RSHIFTEQ = 263,
     LSHIFTEQ = 264,
     DIVEQ = 265,
     MULTEQ = 266,
     MINUSEQ = 267,
     PLUSEQ = 268,
     OROR = 269,
     ANDAND = 270,
     NE = 271,
     EQ = 272,
     GE = 273,
     LE = 274,
     RSHIFT = 275,
     LSHIFT = 276,
     UNARY = 277,
     END = 278,
     ALIGN_K = 279,
     BLOCK = 280,
     BIND = 281,
     QUAD = 282,
     SQUAD = 283,
     LONG = 284,
     SHORT = 285,
     BYTE = 286,
     SECTIONS = 287,
     PHDRS = 288,
     DATA_SEGMENT_ALIGN = 289,
     DATA_SEGMENT_RELRO_END = 290,
     DATA_SEGMENT_END = 291,
     SORT_BY_NAME = 292,
     SORT_BY_ALIGNMENT = 293,
     SIZEOF_HEADERS = 294,
     OUTPUT_FORMAT = 295,
     FORCE_COMMON_ALLOCATION = 296,
     OUTPUT_ARCH = 297,
     INHIBIT_COMMON_ALLOCATION = 298,
     SEGMENT_START = 299,
     INCLUDE = 300,
     MEMORY = 301,
     NOLOAD = 302,
     DSECT = 303,
     COPY = 304,
     INFO = 305,
     OVERLAY = 306,
     DEFINED = 307,
     TARGET_K = 308,
     SEARCH_DIR = 309,
     MAP = 310,
     ENTRY = 311,
     NEXT = 312,
     SIZEOF = 313,
     ALIGNOF = 314,
     ADDR = 315,
     LOADADDR = 316,
     MAX_K = 317,
     MIN_K = 318,
     CEILP2 = 319,
     NACL_MASK = 320,
     STARTUP = 321,
     HLL = 322,
     SYSLIB = 323,
     FLOAT = 324,
     NOFLOAT = 325,
     NOCROSSREFS = 326,
     ORIGIN = 327,
     FILL = 328,
     LENGTH = 329,
     CREATE_OBJECT_SYMBOLS = 330,
     INPUT = 331,
     GROUP = 332,
     OUTPUT = 333,
     CONSTRUCTORS = 334,
     ALIGNMOD = 335,
     AT = 336,
     SUBALIGN = 337,
     PROVIDE = 338,
     PROVIDE_HIDDEN = 339,
     AS_NEEDED = 340,
     CHIP = 341,
     LIST = 342,
     SECT = 343,
     ABSOLUTE = 344,
     LOAD = 345,
     NEWLINE = 346,
     ENDWORD = 347,
     ORDER = 348,
     NAMEWORD = 349,
     ASSERT_K = 350,
     FORMAT = 351,
     PUBLIC = 352,
     DEFSYMEND = 353,
     BASE = 354,
     ALIAS = 355,
     TRUNCATE = 356,
     REL = 357,
     INPUT_SCRIPT = 358,
     INPUT_MRI_SCRIPT = 359,
     INPUT_DEFSYM = 360,
     CASE = 361,
     EXTERN = 362,
     START = 363,
     VERS_TAG = 364,
     VERS_IDENTIFIER = 365,
     GLOBAL = 366,
     LOCAL = 367,
     VERSIONK = 368,
     INPUT_VERSION_SCRIPT = 369,
     KEEP = 370,
     ONLY_IF_RO = 371,
     ONLY_IF_RW = 372,
     SPECIAL = 373,
     EXCLUDE_FILE = 374,
     CONSTANT = 375,
     INPUT_DYNAMIC_LIST = 376
   };
#endif
/* Tokens.  */
#define INT 258
#define NAME 259
#define LNAME 260
#define OREQ 261
#define ANDEQ 262
#define RSHIFTEQ 263
#define LSHIFTEQ 264
#define DIVEQ 265
#define MULTEQ 266
#define MINUSEQ 267
#define PLUSEQ 268
#define OROR 269
#define ANDAND 270
#define NE 271
#define EQ 272
#define GE 273
#define LE 274
#define RSHIFT 275
#define LSHIFT 276
#define UNARY 277
#define END 278
#define ALIGN_K 279
#define BLOCK 280
#define BIND 281
#define QUAD 282
#define SQUAD 283
#define LONG 284
#define SHORT 285
#define BYTE 286
#define SECTIONS 287
#define PHDRS 288
#define DATA_SEGMENT_ALIGN 289
#define DATA_SEGMENT_RELRO_END 290
#define DATA_SEGMENT_END 291
#define SORT_BY_NAME 292
#define SORT_BY_ALIGNMENT 293
#define SIZEOF_HEADERS 294
#define OUTPUT_FORMAT 295
#define FORCE_COMMON_ALLOCATION 296
#define OUTPUT_ARCH 297
#define INHIBIT_COMMON_ALLOCATION 298
#define SEGMENT_START 299
#define INCLUDE 300
#define MEMORY 301
#define NOLOAD 302
#define DSECT 303
#define COPY 304
#define INFO 305
#define OVERLAY 306
#define DEFINED 307
#define TARGET_K 308
#define SEARCH_DIR 309
#define MAP 310
#define ENTRY 311
#define NEXT 312
#define SIZEOF 313
#define ALIGNOF 314
#define ADDR 315
#define LOADADDR 316
#define MAX_K 317
#define MIN_K 318
#define CEILP2 319
#define NACL_MASK 320
#define STARTUP 321
#define HLL 322
#define SYSLIB 323
#define FLOAT 324
#define NOFLOAT 325
#define NOCROSSREFS 326
#define ORIGIN 327
#define FILL 328
#define LENGTH 329
#define CREATE_OBJECT_SYMBOLS 330
#define INPUT 331
#define GROUP 332
#define OUTPUT 333
#define CONSTRUCTORS 334
#define ALIGNMOD 335
#define AT 336
#define SUBALIGN 337
#define PROVIDE 338
#define PROVIDE_HIDDEN 339
#define AS_NEEDED 340
#define CHIP 341
#define LIST 342
#define SECT 343
#define ABSOLUTE 344
#define LOAD 345
#define NEWLINE 346
#define ENDWORD 347
#define ORDER 348
#define NAMEWORD 349
#define ASSERT_K 350
#define FORMAT 351
#define PUBLIC 352
#define DEFSYMEND 353
#define BASE 354
#define ALIAS 355
#define TRUNCATE 356
#define REL 357
#define INPUT_SCRIPT 358
#define INPUT_MRI_SCRIPT 359
#define INPUT_DEFSYM 360
#define CASE 361
#define EXTERN 362
#define START 363
#define VERS_TAG 364
#define VERS_IDENTIFIER 365
#define GLOBAL 366
#define LOCAL 367
#define VERSIONK 368
#define INPUT_VERSION_SCRIPT 369
#define KEEP 370
#define ONLY_IF_RO 371
#define ONLY_IF_RW 372
#define SPECIAL 373
#define EXCLUDE_FILE 374
#define CONSTANT 375
#define INPUT_DYNAMIC_LIST 376




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 66 "ldgram.y"
typedef union YYSTYPE {
  bfd_vma integer;
  struct big_int
    {
      bfd_vma integer;
      char *str;
    } bigint;
  fill_type *fill;
  char *name;
  const char *cname;
  struct wildcard_spec wildcard;
  struct wildcard_list *wildcard_list;
  struct name_list *name_list;
  int token;
  union etree_union *etree;
  struct phdr_info
    {
      bfd_boolean filehdr;
      bfd_boolean phdrs;
      union etree_union *at;
      union etree_union *flags;
    } phdr;
  struct lang_nocrossref *nocrossref;
  struct lang_output_section_phdr_list *section_phdr;
  struct bfd_elf_version_deps *deflist;
  struct bfd_elf_version_expr *versyms;
  struct bfd_elf_version_tree *versnode;
} YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 309 "ldgram.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



