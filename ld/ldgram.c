/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INT = 258,
     NAME = 259,
     LNAME = 260,
     OREQ = 261,
     ANDEQ = 262,
     RSHIFTEQ = 263,
     LSHIFTEQ = 264,
     DIVEQ = 265,
     MULTEQ = 266,
     MINUSEQ = 267,
     PLUSEQ = 268,
     OROR = 269,
     ANDAND = 270,
     NE = 271,
     EQ = 272,
     GE = 273,
     LE = 274,
     RSHIFT = 275,
     LSHIFT = 276,
     UNARY = 277,
     END = 278,
     ALIGN_K = 279,
     BLOCK = 280,
     BIND = 281,
     QUAD = 282,
     SQUAD = 283,
     LONG = 284,
     SHORT = 285,
     BYTE = 286,
     SECTIONS = 287,
     PHDRS = 288,
     DATA_SEGMENT_ALIGN = 289,
     DATA_SEGMENT_RELRO_END = 290,
     DATA_SEGMENT_END = 291,
     SORT_BY_NAME = 292,
     SORT_BY_ALIGNMENT = 293,
     SIZEOF_HEADERS = 294,
     OUTPUT_FORMAT = 295,
     FORCE_COMMON_ALLOCATION = 296,
     OUTPUT_ARCH = 297,
     INHIBIT_COMMON_ALLOCATION = 298,
     SEGMENT_START = 299,
     INCLUDE = 300,
     MEMORY = 301,
     NOLOAD = 302,
     DSECT = 303,
     COPY = 304,
     INFO = 305,
     OVERLAY = 306,
     DEFINED = 307,
     TARGET_K = 308,
     SEARCH_DIR = 309,
     MAP = 310,
     ENTRY = 311,
     NEXT = 312,
     SIZEOF = 313,
     ALIGNOF = 314,
     ADDR = 315,
     LOADADDR = 316,
     MAX_K = 317,
     MIN_K = 318,
     CEILP2 = 319,
     NACL_MASK = 320,
     STARTUP = 321,
     HLL = 322,
     SYSLIB = 323,
     FLOAT = 324,
     NOFLOAT = 325,
     NOCROSSREFS = 326,
     ORIGIN = 327,
     FILL = 328,
     LENGTH = 329,
     CREATE_OBJECT_SYMBOLS = 330,
     INPUT = 331,
     GROUP = 332,
     OUTPUT = 333,
     CONSTRUCTORS = 334,
     ALIGNMOD = 335,
     AT = 336,
     SUBALIGN = 337,
     PROVIDE = 338,
     PROVIDE_HIDDEN = 339,
     AS_NEEDED = 340,
     CHIP = 341,
     LIST = 342,
     SECT = 343,
     ABSOLUTE = 344,
     LOAD = 345,
     NEWLINE = 346,
     ENDWORD = 347,
     ORDER = 348,
     NAMEWORD = 349,
     ASSERT_K = 350,
     FORMAT = 351,
     PUBLIC = 352,
     DEFSYMEND = 353,
     BASE = 354,
     ALIAS = 355,
     TRUNCATE = 356,
     REL = 357,
     INPUT_SCRIPT = 358,
     INPUT_MRI_SCRIPT = 359,
     INPUT_DEFSYM = 360,
     CASE = 361,
     EXTERN = 362,
     START = 363,
     VERS_TAG = 364,
     VERS_IDENTIFIER = 365,
     GLOBAL = 366,
     LOCAL = 367,
     VERSIONK = 368,
     INPUT_VERSION_SCRIPT = 369,
     KEEP = 370,
     ONLY_IF_RO = 371,
     ONLY_IF_RW = 372,
     SPECIAL = 373,
     EXCLUDE_FILE = 374,
     CONSTANT = 375,
     INPUT_DYNAMIC_LIST = 376
   };
#endif
/* Tokens.  */
#define INT 258
#define NAME 259
#define LNAME 260
#define OREQ 261
#define ANDEQ 262
#define RSHIFTEQ 263
#define LSHIFTEQ 264
#define DIVEQ 265
#define MULTEQ 266
#define MINUSEQ 267
#define PLUSEQ 268
#define OROR 269
#define ANDAND 270
#define NE 271
#define EQ 272
#define GE 273
#define LE 274
#define RSHIFT 275
#define LSHIFT 276
#define UNARY 277
#define END 278
#define ALIGN_K 279
#define BLOCK 280
#define BIND 281
#define QUAD 282
#define SQUAD 283
#define LONG 284
#define SHORT 285
#define BYTE 286
#define SECTIONS 287
#define PHDRS 288
#define DATA_SEGMENT_ALIGN 289
#define DATA_SEGMENT_RELRO_END 290
#define DATA_SEGMENT_END 291
#define SORT_BY_NAME 292
#define SORT_BY_ALIGNMENT 293
#define SIZEOF_HEADERS 294
#define OUTPUT_FORMAT 295
#define FORCE_COMMON_ALLOCATION 296
#define OUTPUT_ARCH 297
#define INHIBIT_COMMON_ALLOCATION 298
#define SEGMENT_START 299
#define INCLUDE 300
#define MEMORY 301
#define NOLOAD 302
#define DSECT 303
#define COPY 304
#define INFO 305
#define OVERLAY 306
#define DEFINED 307
#define TARGET_K 308
#define SEARCH_DIR 309
#define MAP 310
#define ENTRY 311
#define NEXT 312
#define SIZEOF 313
#define ALIGNOF 314
#define ADDR 315
#define LOADADDR 316
#define MAX_K 317
#define MIN_K 318
#define CEILP2 319
#define NACL_MASK 320
#define STARTUP 321
#define HLL 322
#define SYSLIB 323
#define FLOAT 324
#define NOFLOAT 325
#define NOCROSSREFS 326
#define ORIGIN 327
#define FILL 328
#define LENGTH 329
#define CREATE_OBJECT_SYMBOLS 330
#define INPUT 331
#define GROUP 332
#define OUTPUT 333
#define CONSTRUCTORS 334
#define ALIGNMOD 335
#define AT 336
#define SUBALIGN 337
#define PROVIDE 338
#define PROVIDE_HIDDEN 339
#define AS_NEEDED 340
#define CHIP 341
#define LIST 342
#define SECT 343
#define ABSOLUTE 344
#define LOAD 345
#define NEWLINE 346
#define ENDWORD 347
#define ORDER 348
#define NAMEWORD 349
#define ASSERT_K 350
#define FORMAT 351
#define PUBLIC 352
#define DEFSYMEND 353
#define BASE 354
#define ALIAS 355
#define TRUNCATE 356
#define REL 357
#define INPUT_SCRIPT 358
#define INPUT_MRI_SCRIPT 359
#define INPUT_DEFSYM 360
#define CASE 361
#define EXTERN 362
#define START 363
#define VERS_TAG 364
#define VERS_IDENTIFIER 365
#define GLOBAL 366
#define LOCAL 367
#define VERSIONK 368
#define INPUT_VERSION_SCRIPT 369
#define KEEP 370
#define ONLY_IF_RO 371
#define ONLY_IF_RW 372
#define SPECIAL 373
#define EXCLUDE_FILE 374
#define CONSTANT 375
#define INPUT_DYNAMIC_LIST 376




/* Copy the first part of user declarations.  */
#line 23 "ldgram.y"

/*

 */

#define DONTDECLARE_MALLOC

#include "sysdep.h"
#include "bfd.h"
#include "bfdlink.h"
#include "ld.h"
#include "ldexp.h"
#include "ldver.h"
#include "ldlang.h"
#include "ldfile.h"
#include "ldemul.h"
#include "ldmisc.h"
#include "ldmain.h"
#include "mri.h"
#include "ldctor.h"
#include "ldlex.h"

#ifndef YYDEBUG
#define YYDEBUG 1
#endif

static enum section_type sectype;
static lang_memory_region_type *region;

FILE *saved_script_handle = NULL;
bfd_boolean force_make_executable = FALSE;

bfd_boolean ldgram_in_script = FALSE;
bfd_boolean ldgram_had_equals = FALSE;
bfd_boolean ldgram_had_keep = FALSE;
char *ldgram_vers_current_lang = NULL;

#define ERROR_NAME_MAX 20
static char *error_names[ERROR_NAME_MAX];
static int error_index;
#define PUSH_ERROR(x) if (error_index < ERROR_NAME_MAX) error_names[error_index] = x; error_index++;
#define POP_ERROR()   error_index--;


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 66 "ldgram.y"
typedef union YYSTYPE {
  bfd_vma integer;
  struct big_int
    {
      bfd_vma integer;
      char *str;
    } bigint;
  fill_type *fill;
  char *name;
  const char *cname;
  struct wildcard_spec wildcard;
  struct wildcard_list *wildcard_list;
  struct name_list *name_list;
  int token;
  union etree_union *etree;
  struct phdr_info
    {
      bfd_boolean filehdr;
      bfd_boolean phdrs;
      union etree_union *at;
      union etree_union *flags;
    } phdr;
  struct lang_nocrossref *nocrossref;
  struct lang_output_section_phdr_list *section_phdr;
  struct bfd_elf_version_deps *deflist;
  struct bfd_elf_version_expr *versyms;
  struct bfd_elf_version_tree *versnode;
} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 400 "ldgram.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 412 "ldgram.c"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  17
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1774

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  145
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  122
/* YYNRULES -- Number of rules. */
#define YYNRULES  348
/* YYNRULES -- Number of states. */
#define YYNSTATES  745

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   376

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   143,     2,     2,     2,    34,    21,     2,
      37,   140,    32,    30,   138,    31,     2,    33,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    16,   139,
      24,     6,    25,    15,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   141,     2,   142,    20,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    53,    19,    54,   144,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     7,     8,     9,    10,    11,    12,    13,    14,    17,
      18,    22,    23,    26,    27,    28,    29,    35,    36,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     6,     9,    12,    15,    18,    20,    21,
      26,    27,    30,    34,    35,    38,    43,    45,    47,    50,
      52,    57,    62,    66,    69,    74,    78,    83,    88,    93,
      98,   103,   106,   109,   112,   117,   122,   125,   128,   131,
     134,   135,   141,   144,   145,   149,   152,   153,   155,   159,
     161,   165,   166,   168,   172,   173,   176,   178,   181,   185,
     186,   189,   192,   193,   195,   197,   199,   201,   203,   205,
     207,   209,   211,   213,   218,   223,   228,   233,   242,   247,
     249,   251,   256,   257,   263,   268,   269,   275,   280,   285,
     287,   291,   294,   296,   300,   303,   304,   310,   311,   319,
     320,   327,   332,   335,   338,   339,   344,   347,   348,   356,
     358,   360,   362,   364,   370,   375,   380,   388,   396,   404,
     412,   421,   424,   426,   430,   432,   434,   438,   443,   445,
     446,   452,   455,   457,   459,   461,   466,   468,   473,   478,
     479,   488,   491,   493,   494,   496,   498,   500,   502,   504,
     506,   508,   511,   512,   514,   516,   518,   520,   522,   524,
     526,   528,   530,   532,   536,   540,   547,   554,   556,   557,
     563,   566,   570,   571,   572,   580,   584,   588,   589,   593,
     595,   598,   600,   603,   608,   613,   617,   621,   623,   628,
     632,   633,   635,   637,   638,   641,   645,   646,   649,   652,
     656,   661,   664,   667,   670,   674,   678,   682,   686,   690,
     694,   698,   702,   706,   710,   714,   718,   722,   726,   730,
     734,   740,   744,   748,   753,   755,   757,   762,   767,   772,
     777,   782,   787,   792,   799,   806,   813,   818,   825,   830,
     832,   839,   846,   851,   856,   863,   868,   873,   877,   878,
     883,   884,   889,   890,   895,   896,   898,   900,   902,   903,
     904,   905,   906,   907,   908,   928,   929,   930,   931,   932,
     933,   952,   953,   954,   962,   964,   966,   968,   970,   972,
     976,   977,   980,   984,   987,   994,  1005,  1008,  1010,  1011,
    1013,  1016,  1017,  1018,  1022,  1023,  1024,  1025,  1026,  1038,
    1043,  1044,  1047,  1048,  1049,  1056,  1058,  1059,  1063,  1069,
    1070,  1074,  1075,  1078,  1080,  1083,  1088,  1091,  1092,  1095,
    1096,  1102,  1104,  1107,  1112,  1118,  1125,  1127,  1130,  1131,
    1134,  1139,  1144,  1153,  1155,  1157,  1161,  1165,  1166,  1176,
    1177,  1185,  1187,  1191,  1193,  1197,  1199,  1203,  1204
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
     146,     0,    -1,   119,   162,    -1,   120,   150,    -1,   130,
     255,    -1,   137,   250,    -1,   121,   148,    -1,     4,    -1,
      -1,   149,     4,     6,   212,    -1,    -1,   151,   152,    -1,
     152,   153,   107,    -1,    -1,   102,   212,    -1,   102,   212,
     138,   212,    -1,     4,    -1,   103,    -1,   109,   155,    -1,
     108,    -1,   113,     4,     6,   212,    -1,   113,     4,   138,
     212,    -1,   113,     4,   212,    -1,   112,     4,    -1,   104,
       4,   138,   212,    -1,   104,     4,   212,    -1,   104,     4,
       6,   212,    -1,    38,     4,     6,   212,    -1,    38,     4,
     138,   212,    -1,    96,     4,     6,   212,    -1,    96,     4,
     138,   212,    -1,   105,   157,    -1,   106,   156,    -1,   110,
       4,    -1,   116,     4,   138,     4,    -1,   116,     4,   138,
       3,    -1,   115,   212,    -1,   117,     3,    -1,   122,   158,
      -1,   123,   159,    -1,    -1,    61,   147,   154,   152,    36,
      -1,   124,     4,    -1,    -1,   155,   138,     4,    -1,   155,
       4,    -1,    -1,     4,    -1,   156,   138,     4,    -1,     4,
      -1,   157,   138,     4,    -1,    -1,     4,    -1,   158,   138,
       4,    -1,    -1,   160,   161,    -1,     4,    -1,   161,     4,
      -1,   161,   138,     4,    -1,    -1,   163,   164,    -1,   164,
     165,    -1,    -1,   194,    -1,   172,    -1,   242,    -1,   203,
      -1,   204,    -1,   206,    -1,   208,    -1,   174,    -1,   257,
      -1,   139,    -1,    69,    37,     4,   140,    -1,    70,    37,
     147,   140,    -1,    94,    37,   147,   140,    -1,    56,    37,
       4,   140,    -1,    56,    37,     4,   138,     4,   138,     4,
     140,    -1,    58,    37,     4,   140,    -1,    57,    -1,    59,
      -1,    92,    37,   168,   140,    -1,    -1,    93,   166,    37,
     168,   140,    -1,    71,    37,   147,   140,    -1,    -1,    61,
     147,   167,   164,    36,    -1,    87,    37,   209,   140,    -1,
     123,    37,   159,   140,    -1,     4,    -1,   168,   138,     4,
      -1,   168,     4,    -1,     5,    -1,   168,   138,     5,    -1,
     168,     5,    -1,    -1,   101,    37,   169,   168,   140,    -1,
      -1,   168,   138,   101,    37,   170,   168,   140,    -1,    -1,
     168,   101,    37,   171,   168,   140,    -1,    46,    53,   173,
      54,    -1,   173,   218,    -1,   173,   174,    -1,    -1,    72,
      37,     4,   140,    -1,   192,   191,    -1,    -1,   111,   175,
      37,   212,   138,     4,   140,    -1,     4,    -1,    32,    -1,
      15,    -1,   176,    -1,   135,    37,   178,   140,   176,    -1,
      51,    37,   176,   140,    -1,    52,    37,   176,   140,    -1,
      51,    37,    52,    37,   176,   140,   140,    -1,    51,    37,
      51,    37,   176,   140,   140,    -1,    52,    37,    51,    37,
     176,   140,   140,    -1,    52,    37,    52,    37,   176,   140,
     140,    -1,    51,    37,   135,    37,   178,   140,   176,   140,
      -1,   178,   176,    -1,   176,    -1,   179,   193,   177,    -1,
     177,    -1,     4,    -1,   141,   179,   142,    -1,   177,    37,
     179,   140,    -1,   180,    -1,    -1,   131,    37,   182,   180,
     140,    -1,   192,   191,    -1,    91,    -1,   139,    -1,    95,
      -1,    51,    37,    95,   140,    -1,   181,    -1,   187,    37,
     210,   140,    -1,    89,    37,   188,   140,    -1,    -1,   111,
     184,    37,   212,   138,     4,   140,   191,    -1,   185,   183,
      -1,   183,    -1,    -1,   185,    -1,    41,    -1,    42,    -1,
      43,    -1,    44,    -1,    45,    -1,   210,    -1,     6,   188,
      -1,    -1,    14,    -1,    13,    -1,    12,    -1,    11,    -1,
      10,    -1,     9,    -1,     8,    -1,     7,    -1,   139,    -1,
     138,    -1,     4,     6,   210,    -1,     4,   190,   210,    -1,
      99,    37,     4,     6,   210,   140,    -1,   100,    37,     4,
       6,   210,   140,    -1,   138,    -1,    -1,    62,    53,   196,
     195,    54,    -1,   195,   196,    -1,   195,   138,   196,    -1,
      -1,    -1,     4,   197,   200,    16,   198,   193,   199,    -1,
      88,     6,   210,    -1,    90,     6,   210,    -1,    -1,    37,
     201,   140,    -1,   202,    -1,   201,   202,    -1,     4,    -1,
     143,     4,    -1,    82,    37,   147,   140,    -1,    83,    37,
     205,   140,    -1,    83,    37,   140,    -1,   205,   193,   147,
      -1,   147,    -1,    84,    37,   207,   140,    -1,   207,   193,
     147,    -1,    -1,    85,    -1,    86,    -1,    -1,     4,   209,
      -1,     4,   138,   209,    -1,    -1,   211,   212,    -1,    31,
     212,    -1,    37,   212,   140,    -1,    73,    37,   212,   140,
      -1,   143,   212,    -1,    30,   212,    -1,   144,   212,    -1,
     212,    32,   212,    -1,   212,    33,   212,    -1,   212,    34,
     212,    -1,   212,    30,   212,    -1,   212,    31,   212,    -1,
     212,    29,   212,    -1,   212,    28,   212,    -1,   212,    23,
     212,    -1,   212,    22,   212,    -1,   212,    27,   212,    -1,
     212,    26,   212,    -1,   212,    24,   212,    -1,   212,    25,
     212,    -1,   212,    21,   212,    -1,   212,    20,   212,    -1,
     212,    19,   212,    -1,   212,    15,   212,    16,   212,    -1,
     212,    18,   212,    -1,   212,    17,   212,    -1,    68,    37,
       4,   140,    -1,     3,    -1,    55,    -1,    75,    37,     4,
     140,    -1,    74,    37,     4,   140,    -1,    76,    37,     4,
     140,    -1,    77,    37,     4,   140,    -1,   136,    37,     4,
     140,    -1,   105,    37,   212,   140,    -1,    38,    37,   212,
     140,    -1,    38,    37,   212,   138,   212,   140,    -1,    48,
      37,   212,   138,   212,   140,    -1,    49,    37,   212,   138,
     212,   140,    -1,    50,    37,   212,   140,    -1,    60,    37,
       4,   138,   212,   140,    -1,    39,    37,   212,   140,    -1,
       4,    -1,    78,    37,   212,   138,   212,   140,    -1,    79,
      37,   212,   138,   212,   140,    -1,    80,    37,   212,   140,
      -1,    81,    37,   212,   140,    -1,   111,    37,   212,   138,
       4,   140,    -1,    88,    37,     4,   140,    -1,    90,    37,
       4,   140,    -1,    97,    25,     4,    -1,    -1,    97,    37,
     212,   140,    -1,    -1,    38,    37,   212,   140,    -1,    -1,
      98,    37,   212,   140,    -1,    -1,   132,    -1,   133,    -1,
     134,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     4,   219,
     233,   214,   215,   216,   220,   217,    53,   221,   186,    54,
     222,   236,   213,   237,   189,   223,   193,    -1,    -1,    -1,
      -1,    -1,    -1,    67,   224,   234,   235,   214,   216,   225,
      53,   226,   238,    54,   227,   236,   213,   237,   189,   228,
     193,    -1,    -1,    -1,    93,   229,   233,   230,    53,   173,
      54,    -1,    63,    -1,    64,    -1,    65,    -1,    66,    -1,
      67,    -1,    37,   231,   140,    -1,    -1,    37,   140,    -1,
     212,   232,    16,    -1,   232,    16,    -1,    40,    37,   212,
     140,   232,    16,    -1,    40,    37,   212,   140,    39,    37,
     212,   140,   232,    16,    -1,   212,    16,    -1,    16,    -1,
      -1,    87,    -1,    25,     4,    -1,    -1,    -1,   237,    16,
       4,    -1,    -1,    -1,    -1,    -1,   238,     4,   239,    53,
     186,    54,   240,   237,   189,   241,   193,    -1,    47,    53,
     243,    54,    -1,    -1,   243,   244,    -1,    -1,    -1,     4,
     245,   247,   248,   246,   139,    -1,   212,    -1,    -1,     4,
     249,   248,    -1,    97,    37,   212,   140,   248,    -1,    -1,
      37,   212,   140,    -1,    -1,   251,   252,    -1,   253,    -1,
     252,   253,    -1,    53,   254,    54,   139,    -1,   263,   139,
      -1,    -1,   256,   259,    -1,    -1,   258,   129,    53,   259,
      54,    -1,   260,    -1,   259,   260,    -1,    53,   262,    54,
     139,    -1,   125,    53,   262,    54,   139,    -1,   125,    53,
     262,    54,   261,   139,    -1,   125,    -1,   261,   125,    -1,
      -1,   263,   139,    -1,   127,    16,   263,   139,    -1,   128,
      16,   263,   139,    -1,   127,    16,   263,   139,   128,    16,
     263,   139,    -1,   126,    -1,     4,    -1,   263,   139,   126,
      -1,   263,   139,     4,    -1,    -1,   263,   139,   123,     4,
      53,   264,   263,   266,    54,    -1,    -1,   123,     4,    53,
     265,   263,   266,    54,    -1,   127,    -1,   263,   139,   127,
      -1,   128,    -1,   263,   139,   128,    -1,   123,    -1,   263,
     139,   123,    -1,    -1,   139,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   164,   164,   165,   166,   167,   168,   172,   176,   176,
     186,   186,   199,   200,   204,   205,   206,   209,   212,   213,
     214,   216,   218,   220,   222,   224,   226,   228,   230,   232,
     234,   236,   237,   238,   240,   242,   244,   246,   248,   249,
     251,   250,   254,   256,   260,   261,   262,   266,   268,   272,
     274,   279,   280,   281,   286,   286,   291,   293,   295,   300,
     300,   306,   307,   312,   313,   314,   315,   316,   317,   318,
     319,   320,   321,   322,   324,   326,   328,   331,   333,   335,
     337,   339,   341,   340,   344,   347,   346,   350,   354,   358,
     361,   364,   367,   370,   373,   377,   376,   381,   380,   385,
     384,   391,   395,   396,   397,   401,   403,   404,   404,   412,
     416,   420,   427,   433,   439,   445,   451,   457,   463,   469,
     475,   484,   493,   504,   513,   524,   532,   536,   543,   545,
     544,   551,   552,   556,   557,   562,   567,   568,   573,   577,
     577,   583,   584,   587,   589,   593,   595,   597,   599,   601,
     606,   613,   615,   619,   621,   623,   625,   627,   629,   631,
     633,   638,   638,   643,   647,   655,   659,   667,   667,   671,
     675,   676,   677,   682,   681,   689,   697,   705,   706,   710,
     711,   715,   717,   722,   727,   728,   733,   735,   741,   743,
     745,   749,   751,   757,   760,   769,   780,   780,   786,   788,
     790,   792,   794,   796,   799,   801,   803,   805,   807,   809,
     811,   813,   815,   817,   819,   821,   823,   825,   827,   829,
     831,   833,   835,   837,   839,   841,   844,   846,   848,   850,
     852,   854,   856,   858,   860,   862,   864,   866,   875,   877,
     879,   881,   883,   885,   887,   889,   891,   897,   898,   902,
     903,   907,   908,   912,   913,   917,   918,   919,   920,   923,
     927,   930,   936,   938,   923,   945,   947,   949,   954,   956,
     944,   966,   968,   966,   976,   977,   978,   979,   980,   984,
     985,   986,   990,   991,   996,   997,  1002,  1003,  1008,  1009,
    1014,  1016,  1021,  1024,  1037,  1041,  1046,  1048,  1039,  1056,
    1059,  1061,  1065,  1066,  1065,  1075,  1120,  1123,  1135,  1144,
    1147,  1154,  1154,  1166,  1167,  1171,  1175,  1184,  1184,  1198,
    1198,  1208,  1209,  1213,  1217,  1221,  1228,  1232,  1240,  1243,
    1247,  1251,  1255,  1262,  1266,  1270,  1274,  1279,  1278,  1292,
    1291,  1301,  1305,  1309,  1313,  1317,  1321,  1327,  1329
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "INT", "NAME", "LNAME", "'='", "OREQ",
  "ANDEQ", "RSHIFTEQ", "LSHIFTEQ", "DIVEQ", "MULTEQ", "MINUSEQ", "PLUSEQ",
  "'?'", "':'", "OROR", "ANDAND", "'|'", "'^'", "'&'", "NE", "EQ", "'<'",
  "'>'", "GE", "LE", "RSHIFT", "LSHIFT", "'+'", "'-'", "'*'", "'/'", "'%'",
  "UNARY", "END", "'('", "ALIGN_K", "BLOCK", "BIND", "QUAD", "SQUAD",
  "LONG", "SHORT", "BYTE", "SECTIONS", "PHDRS", "DATA_SEGMENT_ALIGN",
  "DATA_SEGMENT_RELRO_END", "DATA_SEGMENT_END", "SORT_BY_NAME",
  "SORT_BY_ALIGNMENT", "'{'", "'}'", "SIZEOF_HEADERS", "OUTPUT_FORMAT",
  "FORCE_COMMON_ALLOCATION", "OUTPUT_ARCH", "INHIBIT_COMMON_ALLOCATION",
  "SEGMENT_START", "INCLUDE", "MEMORY", "NOLOAD", "DSECT", "COPY", "INFO",
  "OVERLAY", "DEFINED", "TARGET_K", "SEARCH_DIR", "MAP", "ENTRY", "NEXT",
  "SIZEOF", "ALIGNOF", "ADDR", "LOADADDR", "MAX_K", "MIN_K", "CEILP2",
  "NACL_MASK", "STARTUP", "HLL", "SYSLIB", "FLOAT", "NOFLOAT",
  "NOCROSSREFS", "ORIGIN", "FILL", "LENGTH", "CREATE_OBJECT_SYMBOLS",
  "INPUT", "GROUP", "OUTPUT", "CONSTRUCTORS", "ALIGNMOD", "AT", "SUBALIGN",
  "PROVIDE", "PROVIDE_HIDDEN", "AS_NEEDED", "CHIP", "LIST", "SECT",
  "ABSOLUTE", "LOAD", "NEWLINE", "ENDWORD", "ORDER", "NAMEWORD",
  "ASSERT_K", "FORMAT", "PUBLIC", "DEFSYMEND", "BASE", "ALIAS", "TRUNCATE",
  "REL", "INPUT_SCRIPT", "INPUT_MRI_SCRIPT", "INPUT_DEFSYM", "CASE",
  "EXTERN", "START", "VERS_TAG", "VERS_IDENTIFIER", "GLOBAL", "LOCAL",
  "VERSIONK", "INPUT_VERSION_SCRIPT", "KEEP", "ONLY_IF_RO", "ONLY_IF_RW",
  "SPECIAL", "EXCLUDE_FILE", "CONSTANT", "INPUT_DYNAMIC_LIST", "','",
  "';'", "')'", "'['", "']'", "'!'", "'~'", "$accept", "file", "filename",
  "defsym_expr", "@1", "mri_script_file", "@2", "mri_script_lines",
  "mri_script_command", "@3", "ordernamelist", "mri_load_name_list",
  "mri_abs_name_list", "casesymlist", "extern_name_list", "@4",
  "extern_name_list_body", "script_file", "@5", "ifile_list", "ifile_p1",
  "@6", "@7", "input_list", "@8", "@9", "@10", "sections",
  "sec_or_group_p1", "statement_anywhere", "@11", "wildcard_name",
  "wildcard_spec", "exclude_name_list", "file_NAME_list",
  "input_section_spec_no_keep", "input_section_spec", "@12", "statement",
  "@13", "statement_list", "statement_list_opt", "length", "fill_exp",
  "fill_opt", "assign_op", "end", "assignment", "opt_comma", "memory",
  "memory_spec_list", "memory_spec", "@14", "origin_spec", "length_spec",
  "attributes_opt", "attributes_list", "attributes_string", "startup",
  "high_level_library", "high_level_library_NAME_list",
  "low_level_library", "low_level_library_NAME_list",
  "floating_point_support", "nocrossref_list", "mustbe_exp", "@15", "exp",
  "memspec_at_opt", "opt_at", "opt_align", "opt_subalign",
  "sect_constraint", "section", "@16", "@17", "@18", "@19", "@20", "@21",
  "@22", "@23", "@24", "@25", "@26", "@27", "type", "atype",
  "opt_exp_with_type", "opt_exp_without_type", "opt_nocrossrefs",
  "memspec_opt", "phdr_opt", "overlay_section", "@28", "@29", "@30",
  "phdrs", "phdr_list", "phdr", "@31", "@32", "phdr_type",
  "phdr_qualifiers", "phdr_val", "dynamic_list_file", "@33",
  "dynamic_list_nodes", "dynamic_list_node", "dynamic_list_tag",
  "version_script_file", "@34", "version", "@35", "vers_nodes",
  "vers_node", "verdep", "vers_tag", "vers_defns", "@36", "@37",
  "opt_semicolon", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,    61,   261,   262,   263,
     264,   265,   266,   267,   268,    63,    58,   269,   270,   124,
      94,    38,   271,   272,    60,    62,   273,   274,   275,   276,
      43,    45,    42,    47,    37,   277,   278,    40,   279,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,   293,   123,   125,   294,   295,   296,   297,   298,
     299,   300,   301,   302,   303,   304,   305,   306,   307,   308,
     309,   310,   311,   312,   313,   314,   315,   316,   317,   318,
     319,   320,   321,   322,   323,   324,   325,   326,   327,   328,
     329,   330,   331,   332,   333,   334,   335,   336,   337,   338,
     339,   340,   341,   342,   343,   344,   345,   346,   347,   348,
     349,   350,   351,   352,   353,   354,   355,   356,   357,   358,
     359,   360,   361,   362,   363,   364,   365,   366,   367,   368,
     369,   370,   371,   372,   373,   374,   375,   376,    44,    59,
      41,    91,    93,    33,   126
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short int yyr1[] =
{
       0,   145,   146,   146,   146,   146,   146,   147,   149,   148,
     151,   150,   152,   152,   153,   153,   153,   153,   153,   153,
     153,   153,   153,   153,   153,   153,   153,   153,   153,   153,
     153,   153,   153,   153,   153,   153,   153,   153,   153,   153,
     154,   153,   153,   153,   155,   155,   155,   156,   156,   157,
     157,   158,   158,   158,   160,   159,   161,   161,   161,   163,
     162,   164,   164,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   166,   165,   165,   167,   165,   165,   165,   168,
     168,   168,   168,   168,   168,   169,   168,   170,   168,   171,
     168,   172,   173,   173,   173,   174,   174,   175,   174,   176,
     176,   176,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   178,   178,   179,   179,   180,   180,   180,   181,   182,
     181,   183,   183,   183,   183,   183,   183,   183,   183,   184,
     183,   185,   185,   186,   186,   187,   187,   187,   187,   187,
     188,   189,   189,   190,   190,   190,   190,   190,   190,   190,
     190,   191,   191,   192,   192,   192,   192,   193,   193,   194,
     195,   195,   195,   197,   196,   198,   199,   200,   200,   201,
     201,   202,   202,   203,   204,   204,   205,   205,   206,   207,
     207,   208,   208,   209,   209,   209,   211,   210,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   213,   213,   214,
     214,   215,   215,   216,   216,   217,   217,   217,   217,   219,
     220,   221,   222,   223,   218,   224,   225,   226,   227,   228,
     218,   229,   230,   218,   231,   231,   231,   231,   231,   232,
     232,   232,   233,   233,   233,   233,   234,   234,   235,   235,
     236,   236,   237,   237,   238,   239,   240,   241,   238,   242,
     243,   243,   245,   246,   244,   247,   248,   248,   248,   249,
     249,   251,   250,   252,   252,   253,   254,   256,   255,   258,
     257,   259,   259,   260,   260,   260,   261,   261,   262,   262,
     262,   262,   262,   263,   263,   263,   263,   264,   263,   265,
     263,   263,   263,   263,   263,   263,   263,   266,   266
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     2,     2,     2,     2,     2,     1,     0,     4,
       0,     2,     3,     0,     2,     4,     1,     1,     2,     1,
       4,     4,     3,     2,     4,     3,     4,     4,     4,     4,
       4,     2,     2,     2,     4,     4,     2,     2,     2,     2,
       0,     5,     2,     0,     3,     2,     0,     1,     3,     1,
       3,     0,     1,     3,     0,     2,     1,     2,     3,     0,
       2,     2,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     4,     4,     4,     8,     4,     1,
       1,     4,     0,     5,     4,     0,     5,     4,     4,     1,
       3,     2,     1,     3,     2,     0,     5,     0,     7,     0,
       6,     4,     2,     2,     0,     4,     2,     0,     7,     1,
       1,     1,     1,     5,     4,     4,     7,     7,     7,     7,
       8,     2,     1,     3,     1,     1,     3,     4,     1,     0,
       5,     2,     1,     1,     1,     4,     1,     4,     4,     0,
       8,     2,     1,     0,     1,     1,     1,     1,     1,     1,
       1,     2,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     6,     6,     1,     0,     5,
       2,     3,     0,     0,     7,     3,     3,     0,     3,     1,
       2,     1,     2,     4,     4,     3,     3,     1,     4,     3,
       0,     1,     1,     0,     2,     3,     0,     2,     2,     3,
       4,     2,     2,     2,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       5,     3,     3,     4,     1,     1,     4,     4,     4,     4,
       4,     4,     4,     6,     6,     6,     4,     6,     4,     1,
       6,     6,     4,     4,     6,     4,     4,     3,     0,     4,
       0,     4,     0,     4,     0,     1,     1,     1,     0,     0,
       0,     0,     0,     0,    19,     0,     0,     0,     0,     0,
      18,     0,     0,     7,     1,     1,     1,     1,     1,     3,
       0,     2,     3,     2,     6,    10,     2,     1,     0,     1,
       2,     0,     0,     3,     0,     0,     0,     0,    11,     4,
       0,     2,     0,     0,     6,     1,     0,     3,     5,     0,
       3,     0,     2,     1,     2,     4,     2,     0,     2,     0,
       5,     1,     2,     4,     5,     6,     1,     2,     0,     2,
       4,     4,     8,     1,     1,     3,     3,     0,     9,     0,
       7,     1,     3,     1,     3,     1,     3,     0,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short int yydefact[] =
{
       0,    59,    10,     8,   317,   311,     0,     2,    62,     3,
      13,     6,     0,     4,     0,     5,     0,     1,    60,    11,
       0,   328,     0,   318,   321,     0,   312,   313,     0,     0,
       0,     0,    79,     0,    80,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   191,   192,     0,     0,    82,     0,
       0,     0,   107,     0,    72,    61,    64,    70,     0,    63,
      66,    67,    68,    69,    65,    71,     0,    16,     0,     0,
       0,     0,    17,     0,     0,     0,    19,    46,     0,     0,
       0,     0,     0,     0,    51,    54,     0,     0,     0,   334,
     345,   333,   341,   343,     0,     0,   328,   322,   341,   343,
       0,     0,   314,   196,   160,   159,   158,   157,   156,   155,
     154,   153,   196,   104,   300,     0,     0,     7,    85,     0,
       0,     0,     0,     0,     0,     0,   190,   193,     0,     0,
       0,     0,     0,     0,    54,   162,   161,   106,     0,     0,
      40,     0,   224,   239,     0,     0,     0,     0,     0,     0,
       0,     0,   225,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    14,     0,    49,    31,    47,    32,    18,    33,    23,
       0,    36,     0,    37,    52,    38,    39,     0,    42,    12,
       9,     0,     0,     0,     0,   329,     0,     0,   316,   163,
       0,   164,     0,     0,     0,     0,    62,   173,   172,     0,
       0,     0,     0,     0,   185,   187,   168,   168,   193,     0,
      89,    92,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    13,     0,     0,   202,   198,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     201,   203,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    25,     0,     0,    45,     0,     0,
       0,    22,     0,     0,    56,    55,   339,     0,     0,   323,
     336,   346,   335,   342,   344,     0,   315,   197,   259,   101,
     265,   271,   103,   102,   302,   299,   301,     0,    76,    78,
     319,   177,     0,    73,    74,    84,   105,   183,   167,   184,
       0,   188,     0,   193,   194,    87,    95,    91,    94,     0,
       0,    81,     0,    75,   196,   196,     0,    88,     0,    27,
      28,    43,    29,    30,   199,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   222,   221,   219,
     218,   217,   212,   211,   215,   216,   214,   213,   210,   209,
     207,   208,   204,   205,   206,    15,    26,    24,    50,    48,
      44,    20,    21,    35,    34,    53,    57,     0,     0,   330,
     331,     0,   326,   324,     0,   280,     0,   280,     0,     0,
      86,     0,     0,   169,     0,   170,   186,   189,   195,     0,
      99,    90,    93,     0,    83,     0,     0,     0,   320,    41,
       0,   232,   238,     0,     0,   236,     0,   223,   200,   227,
     226,   228,   229,     0,     0,   242,   243,   245,   246,   231,
       0,   230,     0,    58,   347,   344,   337,   327,   325,     0,
       0,   280,     0,   250,   287,     0,   288,   272,   305,   306,
       0,   181,     0,     0,   179,     0,   171,     0,     0,    97,
     165,   166,     0,     0,     0,     0,     0,     0,     0,     0,
     220,   348,     0,     0,     0,   274,   275,   276,   277,   278,
     281,     0,     0,     0,     0,   283,     0,   252,   286,   289,
     250,     0,   309,     0,   303,     0,   182,   178,   180,     0,
     168,    96,     0,     0,   108,   233,   234,   235,   237,   240,
     241,   244,   340,     0,   347,   279,     0,   282,     0,     0,
     254,   254,   104,     0,   306,     0,     0,    77,   196,     0,
     100,     0,   332,     0,   280,     0,     0,     0,   260,   266,
       0,     0,   307,     0,   304,   175,     0,   174,    98,   338,
       0,     0,   249,     0,     0,   258,     0,   273,   310,   306,
     196,     0,   284,   251,     0,   255,   256,   257,     0,   267,
     308,   176,     0,   253,   261,   294,   280,   143,     0,     0,
     125,   111,   110,   145,   146,   147,   148,   149,     0,     0,
       0,   132,   134,   139,     0,     0,   133,     0,   112,     0,
     128,   136,   142,   144,     0,     0,     0,   295,   268,   285,
       0,     0,   196,     0,   129,     0,   109,     0,   124,   168,
       0,   141,   262,   196,   131,     0,   291,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   150,     0,     0,   122,
       0,     0,   126,     0,   168,   291,     0,   143,     0,   248,
       0,     0,   135,     0,   114,     0,     0,   115,   138,     0,
     109,     0,     0,   121,   123,   127,   248,   137,     0,   290,
       0,   292,     0,     0,     0,     0,     0,     0,   130,   113,
     292,   296,     0,   152,     0,     0,     0,     0,     0,     0,
     152,   292,   247,   196,     0,   269,   117,   116,     0,   118,
     119,     0,   263,   152,   151,   293,   168,   120,   140,   168,
     297,   270,   264,   168,   298
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     6,   118,    11,    12,     9,    10,    19,    87,   233,
     177,   176,   174,   185,   186,   187,   295,     7,     8,    18,
      55,   129,   206,   223,   429,   533,   488,    56,   202,    57,
     133,   628,   629,   670,   649,   630,   631,   668,   632,   643,
     633,   634,   635,   665,   725,   112,   137,    58,   673,    59,
     322,   208,   321,   530,   577,   422,   483,   484,    60,    61,
     216,    62,   217,    63,   219,   666,   200,   238,   701,   517,
     550,   568,   598,   313,   415,   585,   607,   675,   739,   416,
     586,   605,   656,   736,   417,   521,   511,   472,   473,   476,
     520,   679,   713,   608,   655,   721,   743,    64,   203,   316,
     418,   556,   479,   524,   554,    15,    16,    26,    27,   100,
      13,    14,    65,    66,    23,    24,   414,    94,    95,   504,
     408,   502
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -673
static const short int yypact[] =
{
     185,  -673,  -673,  -673,  -673,  -673,    43,  -673,  -673,  -673,
    -673,  -673,    46,  -673,   -11,  -673,    19,  -673,   789,   875,
      63,   119,    51,   -11,  -673,   128,    19,  -673,   884,    66,
      71,   116,  -673,   176,  -673,   123,   125,   194,   201,   213,
     220,   221,   226,   237,  -673,  -673,   249,   250,  -673,   251,
     252,   254,  -673,   255,  -673,  -673,  -673,  -673,    96,  -673,
    -673,  -673,  -673,  -673,  -673,  -673,   164,  -673,   304,   123,
     305,   646,  -673,   309,   310,   312,  -673,  -673,   315,   316,
     319,   646,   320,   322,   325,  -673,   328,   229,   646,  -673,
     336,  -673,   326,   327,   287,   209,   119,  -673,  -673,  -673,
     295,   218,  -673,  -673,  -673,  -673,  -673,  -673,  -673,  -673,
    -673,  -673,  -673,  -673,  -673,   348,   352,  -673,  -673,   357,
     363,   123,   123,   367,   123,    22,  -673,   372,    20,   343,
     123,   383,   384,   353,  -673,  -673,  -673,  -673,   340,    28,
    -673,    41,  -673,  -673,   646,   646,   646,   362,   364,   368,
     369,   376,  -673,   379,   382,   393,   394,   395,   397,   399,
     402,   403,   406,   407,   408,   409,   411,   412,   413,   646,
     646,  1378,   222,  -673,   256,  -673,   262,    35,  -673,  -673,
     347,  1622,   265,  -673,  -673,   313,  -673,   385,  -673,  -673,
    1622,   380,   128,   128,   314,   211,   401,   317,   211,  -673,
     646,  -673,   375,    33,    -3,   324,  -673,  -673,  -673,   329,
     333,   338,   339,   341,  -673,  -673,    76,    89,    36,   354,
    -673,  -673,   428,    16,    20,   356,   460,   470,   646,   361,
     -11,   646,   646,  -673,   646,   646,  -673,  -673,   798,   646,
     646,   646,   646,   646,   476,   488,   646,   498,   499,   504,
     505,   646,   646,   646,   646,   506,   508,   646,   646,   510,
    -673,  -673,   646,   646,   646,   646,   646,   646,   646,   646,
     646,   646,   646,   646,   646,   646,   646,   646,   646,   646,
     646,   646,   646,   646,  1622,   514,   515,  -673,   517,   646,
     646,  1622,   138,   520,  -673,    47,  -673,   386,   389,  -673,
    -673,   525,  -673,  -673,  -673,   -84,  -673,  1622,   884,  -673,
    -673,  -673,  -673,  -673,  -673,  -673,  -673,   527,  -673,  -673,
     863,   495,    82,  -673,  -673,  -673,  -673,  -673,  -673,  -673,
     123,  -673,   123,   372,  -673,  -673,  -673,  -673,  -673,   496,
      24,  -673,    27,  -673,  -673,  -673,  1398,  -673,   -18,  1622,
    1622,  1571,  1622,  1622,  -673,   746,   986,  1418,  1438,  1006,
     415,   398,  1026,   416,   418,   419,   429,  1458,  1500,  1046,
    1066,   433,   434,  1086,  1520,   435,  1681,  1559,  1480,  1716,
     637,  1729,  1740,  1740,   600,   600,   600,   600,   378,   378,
     156,   156,  -673,  -673,  -673,  1622,  1622,  1622,  -673,  -673,
    -673,  1622,  1622,  -673,  -673,  -673,  -673,   530,   128,   236,
     211,   484,  -673,  -673,   -28,   467,   567,   467,   646,   438,
    -673,     4,   523,  -673,   357,  -673,  -673,  -673,  -673,    20,
    -673,  -673,  -673,   540,  -673,   441,   442,   547,  -673,  -673,
     646,  -673,  -673,   646,   646,  -673,   646,  -673,  -673,  -673,
    -673,  -673,  -673,   646,   646,  -673,  -673,  -673,  -673,  -673,
     580,  -673,   646,  -673,   446,   570,  -673,  -673,  -673,    15,
     550,  1593,   572,   502,  -673,  1701,   513,  -673,  1622,    18,
     589,  -673,   598,     3,  -673,   519,  -673,   129,    20,  -673,
    -673,  -673,   468,  1110,  1130,  1150,  1170,  1190,  1210,   469,
    1622,   211,   558,   128,   128,  -673,  -673,  -673,  -673,  -673,
    -673,   474,   646,   396,   602,  -673,   582,   583,  -673,  -673,
     502,   571,   586,   599,  -673,   480,  -673,  -673,  -673,   620,
     500,  -673,   135,    20,  -673,  -673,  -673,  -673,  -673,  -673,
    -673,  -673,  -673,   512,   446,  -673,  1234,  -673,   646,   615,
     539,   539,  -673,   646,    18,   646,   534,  -673,  -673,   549,
    -673,   140,   211,   621,   225,  1254,   646,   616,  -673,  -673,
     400,  1274,  -673,  1294,  -673,  -673,   648,  -673,  -673,  -673,
     619,   658,  -673,  1314,   646,    90,   626,  -673,  -673,    18,
    -673,   646,  -673,  -673,  1334,  -673,  -673,  -673,   627,  -673,
    -673,  -673,  1358,  -673,  -673,  -673,   644,   703,    58,   666,
     554,  -673,  -673,  -673,  -673,  -673,  -673,  -673,   649,   650,
     651,  -673,  -673,  -673,   652,   653,  -673,   279,  -673,   654,
    -673,  -673,  -673,   703,   638,   656,    96,  -673,  -673,  -673,
     233,   366,  -673,   660,  -673,   217,  -673,   661,  -673,   -20,
     279,  -673,  -673,  -673,  -673,   647,   674,   665,   667,   565,
     671,   569,   675,   676,   575,   576,  -673,   646,    98,  -673,
      12,   303,  -673,   279,   141,   674,   577,   703,   724,   632,
     217,   217,  -673,   217,  -673,   217,   217,  -673,  -673,  1540,
     590,   591,   217,  -673,  -673,  -673,   632,  -673,   678,  -673,
     708,  -673,   597,   601,    44,   603,   609,   734,  -673,  -673,
    -673,  -673,   735,    61,   610,   612,   217,   613,   618,   622,
      61,  -673,  -673,  -673,   736,  -673,  -673,  -673,   641,  -673,
    -673,    96,  -673,    61,  -673,  -673,   500,  -673,  -673,   500,
    -673,  -673,  -673,   500,  -673
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -673,  -673,   -64,  -673,  -673,  -673,  -673,   509,  -673,  -673,
    -673,  -673,  -673,  -673,   625,  -673,  -673,  -673,  -673,   578,
    -673,  -673,  -673,  -212,  -673,  -673,  -673,  -673,   204,  -196,
    -673,   -91,  -542,    77,   133,   117,  -673,  -673,   153,  -673,
    -673,   110,  -673,    65,  -649,  -673,  -621,  -577,  -213,  -673,
    -673,  -308,  -673,  -673,  -673,  -673,  -673,   308,  -673,  -673,
    -673,  -673,  -673,  -673,  -195,  -101,  -673,   -71,    99,   276,
    -673,   246,  -673,  -673,  -673,  -673,  -673,  -673,  -673,  -673,
    -673,  -673,  -673,  -673,  -673,  -673,  -673,  -458,   387,  -673,
    -673,   124,  -672,  -673,  -673,  -673,  -673,  -673,  -673,  -673,
    -673,  -673,  -673,  -521,  -673,  -673,  -673,  -673,   774,  -673,
    -673,  -673,  -673,  -673,   579,   -22,  -673,   705,   -16,  -673,
    -673,   261
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -320
static const short int yytable[] =
{
     171,    97,   199,   330,   332,   140,   312,   481,   481,   101,
     181,   201,   342,   514,   425,   654,   646,   190,   142,   143,
     337,   338,   522,   334,   220,   221,   117,   611,   431,   432,
     636,   337,   338,   572,   231,    21,   438,   314,   720,   287,
     218,   412,    21,    17,   612,   144,   145,   234,   646,   733,
      20,   406,   146,   147,   148,   413,   636,   210,   211,   611,
     213,   215,   637,   149,   150,   151,   225,   723,   600,    88,
     152,   732,    25,   236,   237,   153,   612,   724,   505,   506,
     507,   508,   509,   154,   740,   648,   207,   315,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   467,   260,   261,
     636,   284,   690,   164,    96,   165,   581,    22,   648,   291,
     738,   468,   638,   611,    22,   523,   486,   339,   328,   113,
     166,   222,   672,    89,   114,   433,   167,   117,   339,   307,
     612,   694,    89,   337,   338,   317,   423,   318,   428,   337,
     338,   403,   404,   527,   337,   338,   482,   482,   609,   647,
     619,   168,   692,   115,   340,   510,   341,   346,   169,   170,
     349,   350,   214,   352,   353,   340,   232,   434,   355,   356,
     357,   358,   359,   288,   333,   362,   297,   298,   119,   235,
     367,   368,   369,   370,   716,   407,   373,   374,   278,   279,
     280,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   116,   328,   300,   329,   487,   401,   402,
     424,   646,   595,   596,   597,   142,   143,   328,   282,   331,
     339,   120,   611,   625,   135,   136,   339,   646,   121,   627,
     300,   339,    90,   435,   436,    91,    92,    93,   611,   612,
     122,    90,   144,   145,    91,    98,    99,   123,   124,   146,
     147,   148,   513,   125,   580,   612,   426,   340,   427,   531,
     149,   150,   151,   340,   126,   560,   532,   152,   340,   328,
     578,   695,   153,   646,   657,   658,   127,   128,   130,   131,
     154,   132,   134,   138,   611,   155,   156,   157,   158,   159,
     160,   161,   162,   163,     1,     2,     3,   646,   139,   141,
     164,   612,   165,   172,   173,     4,   175,   559,   611,   178,
     179,   561,     5,   180,   182,   183,    97,   166,   659,   184,
     647,   619,   188,   167,   301,   612,   189,   302,   303,   304,
     191,   194,   192,   193,   471,   475,   471,   478,   195,   197,
     142,   143,   204,   289,   657,   658,   205,   198,   168,   301,
     283,   207,   302,   303,   465,   169,   170,   209,   660,   493,
     646,   212,   494,   495,   312,   496,   218,   144,   145,   308,
     224,   611,   497,   498,   146,   147,   148,   226,   227,   294,
     228,   500,   464,   230,   285,   149,   150,   151,   612,   239,
     286,   240,   152,   292,   308,   241,   242,   153,   276,   277,
     278,   279,   280,   243,   625,   154,   244,   662,   663,   245,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   309,
     246,   247,   248,   296,   249,   164,   250,   165,   660,   251,
     252,   546,   310,   253,   254,   255,   256,    40,   257,   258,
     259,   293,   166,   299,   587,   305,   306,   575,   167,   505,
     506,   507,   508,   509,   319,   336,   344,   310,   311,   323,
     142,   143,    40,   324,    50,    51,   345,   565,   325,   326,
     360,   327,   571,   168,   573,   290,    52,   543,   544,   601,
     169,   170,   361,   311,   335,   583,   343,   144,   145,    50,
      51,   347,   363,   364,   469,   147,   148,   470,   365,   366,
     371,    52,   372,   594,   375,   149,   150,   151,   398,   399,
     602,   400,   152,   741,   405,   409,   742,   153,   410,   411,
     744,   419,   421,   430,   463,   154,   510,   466,   447,   485,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   661,
     664,   492,   676,   446,   669,   164,   449,   165,   450,   451,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   452,
     142,   143,   166,   457,   458,   461,   480,   489,   167,   693,
     661,   490,   491,   474,   499,   501,   503,   512,   515,   702,
     703,  -109,   669,   525,   705,   706,   689,   144,   145,   516,
     519,   709,   526,   168,   146,   147,   148,   529,   534,   541,
     169,   170,   542,   693,   545,   149,   150,   151,   547,   548,
     557,   549,   152,   553,   552,   728,   558,   153,   274,   275,
     276,   277,   278,   279,   280,   154,   555,   567,   328,   576,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   142,
     143,   562,   566,   584,   590,   164,   591,   165,   267,   268,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,   166,   574,   592,   579,   144,   145,   167,   599,
     604,   513,   639,   146,   147,   148,   640,   641,   642,   644,
     645,   650,   652,   653,   149,   150,   151,   667,   671,   678,
     677,   152,   680,   168,   681,   682,   153,   610,   683,   684,
     169,   170,   685,   686,   154,   687,   688,   697,   611,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   699,   700,
    -125,   708,   711,   712,   164,   612,   165,   714,   719,   722,
     735,   715,   351,   717,   613,   614,   615,   616,   617,   718,
     726,   166,   727,   729,   618,   619,   570,   167,   730,   229,
     704,   262,   731,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   737,   168,   674,   320,   691,   651,   698,   734,   169,
     170,   528,   620,    28,   621,   710,   551,   569,   622,   696,
     102,   196,    50,    51,   477,   563,     0,     0,     0,   348,
       0,     0,     0,   262,   623,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     0,   624,    29,    30,     0,   625,     0,
       0,     0,   626,     0,   627,    31,    32,    33,    34,     0,
      35,    36,     0,     0,     0,     0,     0,     0,    37,    38,
      39,    40,     0,     0,     0,     0,     0,    28,     0,     0,
       0,    41,    42,    43,    44,    45,    46,     0,     0,    67,
       0,    47,    48,    49,   440,     0,   441,     0,    50,    51,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   420,
      52,     0,     0,     0,     0,     0,     0,     0,     0,    29,
      30,     0,    53,    68,     0,     0,     0,     0,  -319,    31,
      32,    33,    34,     0,    35,    36,     0,     0,    54,     0,
       0,     0,    37,    38,    39,    40,    69,     0,   354,     0,
       0,     0,     0,     0,     0,    41,    42,    43,    44,    45,
      46,     0,     0,     0,     0,    47,    48,    49,     0,     0,
       0,     0,    50,    51,     0,     0,     0,     0,     0,     0,
       0,    70,     0,     0,    52,     0,     0,    71,    72,    73,
      74,    75,   -43,    76,    77,    78,    53,    79,    80,     0,
      81,    82,    83,     0,     0,     0,     0,    84,    85,    86,
       0,   262,    54,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   262,     0,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   262,     0,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   262,     0,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   262,     0,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   262,     0,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,     0,     0,     0,     0,   262,   442,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   445,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   448,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   455,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   456,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   459,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,     0,     0,     0,     0,   262,
     535,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   262,
     536,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   262,
     537,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   262,
     538,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   262,
     539,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   262,
     540,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,     0,
       0,     0,     0,   262,   564,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   262,   582,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   262,   588,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   262,   589,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   262,   593,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   262,   603,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     0,     0,     0,     0,     0,   606,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   281,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   437,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   262,   443,   263,   264,   265,
     266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    67,   444,   264,   265,   266,
     267,   268,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,     0,     0,   453,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   439,   262,    68,
     263,   264,   265,   266,   267,   268,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,     0,     0,
     513,     0,    69,     0,     0,     0,     0,   262,   454,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,     0,   460,     0,
       0,     0,     0,     0,     0,     0,     0,    70,     0,     0,
       0,     0,     0,    71,    72,    73,    74,    75,   707,    76,
      77,    78,     0,    79,    80,     0,    81,    82,    83,     0,
       0,     0,     0,    84,    85,    86,   262,   462,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   262,   518,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   268,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280
};

static const short int yycheck[] =
{
      71,    23,   103,   216,   217,    69,   202,     4,     4,    25,
      81,   112,   224,   471,   322,   636,     4,    88,     3,     4,
       4,     5,     4,   218,     4,     5,     4,    15,     4,     5,
     607,     4,     5,   554,     6,    53,    54,     4,   710,     4,
       4,   125,    53,     0,    32,    30,    31,     6,     4,   721,
       4,     4,    37,    38,    39,   139,   633,   121,   122,    15,
     124,   125,     4,    48,    49,    50,   130,     6,   589,     6,
      55,   720,    53,   144,   145,    60,    32,    16,    63,    64,
      65,    66,    67,    68,   733,   627,     4,    54,    73,    74,
      75,    76,    77,    78,    79,    80,    81,   125,   169,   170,
     677,   172,     4,    88,    53,    90,   564,   125,   650,   180,
     731,   139,    54,    15,   125,    97,   424,   101,   138,    53,
     105,   101,   142,     4,    53,   101,   111,     4,   101,   200,
      32,   673,     4,     4,     5,   138,    54,   140,   333,     4,
       5,     3,     4,   140,     4,     5,   143,   143,   606,    51,
      52,   136,   140,    37,   138,   140,   140,   228,   143,   144,
     231,   232,   140,   234,   235,   138,   138,   140,   239,   240,
     241,   242,   243,   138,   138,   246,   192,   193,    53,   138,
     251,   252,   253,   254,   140,   138,   257,   258,    32,    33,
      34,   262,   263,   264,   265,   266,   267,   268,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
     281,   282,   283,    37,   138,     4,   140,   429,   289,   290,
     138,     4,   132,   133,   134,     3,     4,   138,     6,   140,
     101,    37,    15,   135,   138,   139,   101,     4,    37,   141,
       4,   101,   123,   344,   345,   126,   127,   128,    15,    32,
      37,   123,    30,    31,   126,   127,   128,    37,    37,    37,
      38,    39,    37,    37,    39,    32,   330,   138,   332,   140,
      48,    49,    50,   138,    37,   140,   488,    55,   138,   138,
     140,   140,    60,     4,    51,    52,    37,    37,    37,    37,
      68,    37,    37,   129,    15,    73,    74,    75,    76,    77,
      78,    79,    80,    81,   119,   120,   121,     4,     4,     4,
      88,    32,    90,     4,     4,   130,     4,   530,    15,     4,
       4,   533,   137,     4,     4,     3,   348,   105,    95,     4,
      51,    52,     4,   111,   123,    32,   107,   126,   127,   128,
       4,    54,    16,    16,   415,   416,   417,   418,   139,    54,
       3,     4,     4,     6,    51,    52,     4,   139,   136,   123,
     138,     4,   126,   127,   128,   143,   144,     4,   135,   440,
       4,     4,   443,   444,   570,   446,     4,    30,    31,     4,
      37,    15,   453,   454,    37,    38,    39,     4,     4,     4,
      37,   462,   408,    53,   138,    48,    49,    50,    32,    37,
     138,    37,    55,   138,     4,    37,    37,    60,    30,    31,
      32,    33,    34,    37,   135,    68,    37,    51,    52,    37,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    54,
      37,    37,    37,    53,    37,    88,    37,    90,   135,    37,
      37,   512,    67,    37,    37,    37,    37,    72,    37,    37,
      37,   138,   105,   139,    54,    54,   139,   558,   111,    63,
      64,    65,    66,    67,   140,    37,     6,    67,    93,   140,
       3,     4,    72,   140,    99,   100,     6,   548,   140,   140,
       4,   140,   553,   136,   555,   138,   111,   503,   504,   590,
     143,   144,     4,    93,   140,   566,   140,    30,    31,    99,
     100,   140,     4,     4,    37,    38,    39,    40,     4,     4,
       4,   111,     4,   584,     4,    48,    49,    50,     4,     4,
     591,     4,    55,   736,     4,   139,   739,    60,   139,     4,
     743,     4,    37,    37,     4,    68,   140,    53,   140,    16,
      73,    74,    75,    76,    77,    78,    79,    80,    81,   640,
     641,     4,   653,   138,   645,    88,   140,    90,   140,   140,
       6,     7,     8,     9,    10,    11,    12,    13,    14,   140,
       3,     4,   105,   140,   140,   140,   138,    37,   111,   670,
     671,   140,   140,    16,     4,   139,    16,    37,    16,   680,
     681,    37,   683,     4,   685,   686,   667,    30,    31,    97,
      87,   692,     4,   136,    37,    38,    39,    88,   140,   140,
     143,   144,    54,   704,   140,    48,    49,    50,    16,    37,
     140,    38,    55,    37,    53,   716,     6,    60,    28,    29,
      30,    31,    32,    33,    34,    68,    37,    98,   138,    90,
      73,    74,    75,    76,    77,    78,    79,    80,    81,     3,
       4,   139,    37,    37,     6,    88,    37,    90,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,   105,   139,    16,    54,    30,    31,   111,    53,
      53,    37,    16,    37,    38,    39,    37,    37,    37,    37,
      37,    37,    54,    37,    48,    49,    50,    37,    37,    25,
      53,    55,    37,   136,    37,   140,    60,     4,    37,   140,
     143,   144,    37,    37,    68,   140,   140,   140,    15,    73,
      74,    75,    76,    77,    78,    79,    80,    81,     4,    97,
     140,   140,    54,    25,    88,    32,    90,   140,     4,     4,
       4,   140,   233,   140,    41,    42,    43,    44,    45,   140,
     140,   105,   140,   140,    51,    52,   552,   111,   140,   134,
     683,    15,   140,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,   140,   136,   650,   206,   668,   633,   677,   723,   143,
     144,   483,    89,     4,    91,   696,   520,   551,    95,   675,
      26,    96,    99,   100,   417,   544,    -1,    -1,    -1,   230,
      -1,    -1,    -1,    15,   111,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    -1,   131,    46,    47,    -1,   135,    -1,
      -1,    -1,   139,    -1,   141,    56,    57,    58,    59,    -1,
      61,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,
      71,    72,    -1,    -1,    -1,    -1,    -1,     4,    -1,    -1,
      -1,    82,    83,    84,    85,    86,    87,    -1,    -1,     4,
      -1,    92,    93,    94,   138,    -1,   140,    -1,    99,   100,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    36,
     111,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    -1,   123,    38,    -1,    -1,    -1,    -1,   129,    56,
      57,    58,    59,    -1,    61,    62,    -1,    -1,   139,    -1,
      -1,    -1,    69,    70,    71,    72,    61,    -1,   140,    -1,
      -1,    -1,    -1,    -1,    -1,    82,    83,    84,    85,    86,
      87,    -1,    -1,    -1,    -1,    92,    93,    94,    -1,    -1,
      -1,    -1,    99,   100,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    96,    -1,    -1,   111,    -1,    -1,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   123,   112,   113,    -1,
     115,   116,   117,    -1,    -1,    -1,    -1,   122,   123,   124,
      -1,    15,   139,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    15,    -1,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    15,    -1,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    15,    -1,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    15,    -1,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    15,    -1,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    -1,    -1,    -1,    -1,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   140,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    -1,    -1,    -1,    -1,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    15,
     140,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    -1,
      -1,    -1,    -1,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    15,   140,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    -1,    -1,    -1,    -1,    -1,   140,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   138,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   138,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    15,   138,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,     4,   138,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    -1,    -1,   138,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    36,    15,    38,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    -1,    -1,
      37,    -1,    61,    -1,    -1,    -1,    -1,    15,   138,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    -1,   138,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,
      -1,    -1,    -1,   102,   103,   104,   105,   106,   138,   108,
     109,   110,    -1,   112,   113,    -1,   115,   116,   117,    -1,
      -1,    -1,    -1,   122,   123,   124,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short int yystos[] =
{
       0,   119,   120,   121,   130,   137,   146,   162,   163,   150,
     151,   148,   149,   255,   256,   250,   251,     0,   164,   152,
       4,    53,   125,   259,   260,    53,   252,   253,     4,    46,
      47,    56,    57,    58,    59,    61,    62,    69,    70,    71,
      72,    82,    83,    84,    85,    86,    87,    92,    93,    94,
      99,   100,   111,   123,   139,   165,   172,   174,   192,   194,
     203,   204,   206,   208,   242,   257,   258,     4,    38,    61,
      96,   102,   103,   104,   105,   106,   108,   109,   110,   112,
     113,   115,   116,   117,   122,   123,   124,   153,     6,     4,
     123,   126,   127,   128,   262,   263,    53,   260,   127,   128,
     254,   263,   253,     6,     7,     8,     9,    10,    11,    12,
      13,    14,   190,    53,    53,    37,    37,     4,   147,    53,
      37,    37,    37,    37,    37,    37,    37,    37,    37,   166,
      37,    37,    37,   175,    37,   138,   139,   191,   129,     4,
     147,     4,     3,     4,    30,    31,    37,    38,    39,    48,
      49,    50,    55,    60,    68,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    88,    90,   105,   111,   136,   143,
     144,   212,     4,     4,   157,     4,   156,   155,     4,     4,
       4,   212,     4,     3,     4,   158,   159,   160,     4,   107,
     212,     4,    16,    16,    54,   139,   262,    54,   139,   210,
     211,   210,   173,   243,     4,     4,   167,     4,   196,     4,
     147,   147,     4,   147,   140,   147,   205,   207,     4,   209,
       4,     5,   101,   168,    37,   147,     4,     4,    37,   159,
      53,     6,   138,   154,     6,   138,   212,   212,   212,    37,
      37,    37,    37,    37,    37,    37,    37,    37,    37,    37,
      37,    37,    37,    37,    37,    37,    37,    37,    37,    37,
     212,   212,    15,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,   138,     6,   138,   212,   138,   138,     4,   138,     6,
     138,   212,   138,   138,     4,   161,    53,   263,   263,   139,
       4,   123,   126,   127,   128,    54,   139,   212,     4,    54,
      67,    93,   174,   218,     4,    54,   244,   138,   140,   140,
     164,   197,   195,   140,   140,   140,   140,   140,   138,   140,
     193,   140,   193,   138,   209,   140,    37,     4,     5,   101,
     138,   140,   168,   140,     6,     6,   212,   140,   259,   212,
     212,   152,   212,   212,   140,   212,   212,   212,   212,   212,
       4,     4,   212,     4,     4,     4,     4,   212,   212,   212,
     212,     4,     4,   212,   212,     4,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,   212,   212,
     212,   212,   212,   212,   212,   212,   212,   212,     4,     4,
       4,   212,   212,     3,     4,     4,     4,   138,   265,   139,
     139,     4,   125,   139,   261,   219,   224,   229,   245,     4,
      36,    37,   200,    54,   138,   196,   147,   147,   209,   169,
      37,     4,     5,   101,   140,   210,   210,   138,    54,    36,
     138,   140,   140,   138,   138,   140,   138,   140,   140,   140,
     140,   140,   140,   138,   138,   140,   140,   140,   140,   140,
     138,   140,    16,     4,   263,   128,    53,   125,   139,    37,
      40,   212,   232,   233,    16,   212,   234,   233,   212,   247,
     138,     4,   143,   201,   202,    16,   196,   168,   171,    37,
     140,   140,     4,   212,   212,   212,   212,   212,   212,     4,
     212,   139,   266,    16,   264,    63,    64,    65,    66,    67,
     140,   231,    37,    37,   232,    16,    97,   214,    16,    87,
     235,   230,     4,    97,   248,     4,     4,   140,   202,    88,
     198,   140,   168,   170,   140,   140,   140,   140,   140,   140,
     140,   140,    54,   263,   263,   140,   212,    16,    37,    38,
     215,   214,    53,    37,   249,    37,   246,   140,     6,   193,
     140,   168,   139,   266,   140,   212,    37,    98,   216,   216,
     173,   212,   248,   212,   139,   210,    90,   199,   140,    54,
      39,   232,   140,   212,    37,   220,   225,    54,   140,   140,
       6,    37,    16,   140,   212,   132,   133,   134,   217,    53,
     248,   210,   212,   140,    53,   226,   140,   221,   238,   232,
       4,    15,    32,    41,    42,    43,    44,    45,    51,    52,
      89,    91,    95,   111,   131,   135,   139,   141,   176,   177,
     180,   181,   183,   185,   186,   187,   192,     4,    54,    16,
      37,    37,    37,   184,    37,    37,     4,    51,   177,   179,
      37,   183,    54,    37,   191,   239,   227,    51,    52,    95,
     135,   176,    51,    52,   176,   188,   210,    37,   182,   176,
     178,    37,   142,   193,   179,   222,   210,    53,    25,   236,
      37,    37,   140,    37,   140,    37,    37,   140,   140,   212,
       4,   180,   140,   176,   177,   140,   236,   140,   186,     4,
      97,   213,   176,   176,   178,   176,   176,   138,   140,   176,
     213,    54,    25,   237,   140,   140,   140,   140,   140,     4,
     237,   240,     4,     6,    16,   189,   140,   140,   176,   140,
     140,   140,   189,   237,   188,     4,   228,   140,   191,   223,
     189,   193,   193,   241,   193
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()
    ;
#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 8:
#line 176 "ldgram.y"
    { ldlex_defsym(); }
    break;

  case 9:
#line 178 "ldgram.y"
    {
		  ldlex_popstate();
		  lang_add_assignment(exp_assop((yyvsp[-1].token),(yyvsp[-2].name),(yyvsp[0].etree)));
		}
    break;

  case 10:
#line 186 "ldgram.y"
    {
		  ldlex_mri_script ();
		  PUSH_ERROR (_("MRI style script"));
		}
    break;

  case 11:
#line 191 "ldgram.y"
    {
		  ldlex_popstate ();
		  mri_draw_tree ();
		  POP_ERROR ();
		}
    break;

  case 16:
#line 206 "ldgram.y"
    {
			einfo(_("%P%F: unrecognised keyword in MRI style script '%s'\n"),(yyvsp[0].name));
			}
    break;

  case 17:
#line 209 "ldgram.y"
    {
			config.map_filename = "-";
			}
    break;

  case 20:
#line 215 "ldgram.y"
    { mri_public((yyvsp[-2].name), (yyvsp[0].etree)); }
    break;

  case 21:
#line 217 "ldgram.y"
    { mri_public((yyvsp[-2].name), (yyvsp[0].etree)); }
    break;

  case 22:
#line 219 "ldgram.y"
    { mri_public((yyvsp[-1].name), (yyvsp[0].etree)); }
    break;

  case 23:
#line 221 "ldgram.y"
    { mri_format((yyvsp[0].name)); }
    break;

  case 24:
#line 223 "ldgram.y"
    { mri_output_section((yyvsp[-2].name), (yyvsp[0].etree));}
    break;

  case 25:
#line 225 "ldgram.y"
    { mri_output_section((yyvsp[-1].name), (yyvsp[0].etree));}
    break;

  case 26:
#line 227 "ldgram.y"
    { mri_output_section((yyvsp[-2].name), (yyvsp[0].etree));}
    break;

  case 27:
#line 229 "ldgram.y"
    { mri_align((yyvsp[-2].name),(yyvsp[0].etree)); }
    break;

  case 28:
#line 231 "ldgram.y"
    { mri_align((yyvsp[-2].name),(yyvsp[0].etree)); }
    break;

  case 29:
#line 233 "ldgram.y"
    { mri_alignmod((yyvsp[-2].name),(yyvsp[0].etree)); }
    break;

  case 30:
#line 235 "ldgram.y"
    { mri_alignmod((yyvsp[-2].name),(yyvsp[0].etree)); }
    break;

  case 33:
#line 239 "ldgram.y"
    { mri_name((yyvsp[0].name)); }
    break;

  case 34:
#line 241 "ldgram.y"
    { mri_alias((yyvsp[-2].name),(yyvsp[0].name),0);}
    break;

  case 35:
#line 243 "ldgram.y"
    { mri_alias ((yyvsp[-2].name), 0, (int) (yyvsp[0].bigint).integer); }
    break;

  case 36:
#line 245 "ldgram.y"
    { mri_base((yyvsp[0].etree)); }
    break;

  case 37:
#line 247 "ldgram.y"
    { mri_truncate ((unsigned int) (yyvsp[0].bigint).integer); }
    break;

  case 40:
#line 251 "ldgram.y"
    { ldlex_script (); ldfile_open_command_file((yyvsp[0].name)); }
    break;

  case 41:
#line 253 "ldgram.y"
    { ldlex_popstate (); }
    break;

  case 42:
#line 255 "ldgram.y"
    { lang_add_entry ((yyvsp[0].name), FALSE); }
    break;

  case 44:
#line 260 "ldgram.y"
    { mri_order((yyvsp[0].name)); }
    break;

  case 45:
#line 261 "ldgram.y"
    { mri_order((yyvsp[0].name)); }
    break;

  case 47:
#line 267 "ldgram.y"
    { mri_load((yyvsp[0].name)); }
    break;

  case 48:
#line 268 "ldgram.y"
    { mri_load((yyvsp[0].name)); }
    break;

  case 49:
#line 273 "ldgram.y"
    { mri_only_load((yyvsp[0].name)); }
    break;

  case 50:
#line 275 "ldgram.y"
    { mri_only_load((yyvsp[0].name)); }
    break;

  case 51:
#line 279 "ldgram.y"
    { (yyval.name) = NULL; }
    break;

  case 54:
#line 286 "ldgram.y"
    { ldlex_expression (); }
    break;

  case 55:
#line 288 "ldgram.y"
    { ldlex_popstate (); }
    break;

  case 56:
#line 292 "ldgram.y"
    { ldlang_add_undef ((yyvsp[0].name)); }
    break;

  case 57:
#line 294 "ldgram.y"
    { ldlang_add_undef ((yyvsp[0].name)); }
    break;

  case 58:
#line 296 "ldgram.y"
    { ldlang_add_undef ((yyvsp[0].name)); }
    break;

  case 59:
#line 300 "ldgram.y"
    { ldlex_both(); }
    break;

  case 60:
#line 302 "ldgram.y"
    { ldlex_popstate(); }
    break;

  case 73:
#line 323 "ldgram.y"
    { lang_add_target((yyvsp[-1].name)); }
    break;

  case 74:
#line 325 "ldgram.y"
    { ldfile_add_library_path ((yyvsp[-1].name), FALSE); }
    break;

  case 75:
#line 327 "ldgram.y"
    { lang_add_output((yyvsp[-1].name), 1); }
    break;

  case 76:
#line 329 "ldgram.y"
    { lang_add_output_format ((yyvsp[-1].name), (char *) NULL,
					    (char *) NULL, 1); }
    break;

  case 77:
#line 332 "ldgram.y"
    { lang_add_output_format ((yyvsp[-5].name), (yyvsp[-3].name), (yyvsp[-1].name), 1); }
    break;

  case 78:
#line 334 "ldgram.y"
    { ldfile_set_output_arch ((yyvsp[-1].name), bfd_arch_unknown); }
    break;

  case 79:
#line 336 "ldgram.y"
    { command_line.force_common_definition = TRUE ; }
    break;

  case 80:
#line 338 "ldgram.y"
    { command_line.inhibit_common_definition = TRUE ; }
    break;

  case 82:
#line 341 "ldgram.y"
    { lang_enter_group (); }
    break;

  case 83:
#line 343 "ldgram.y"
    { lang_leave_group (); }
    break;

  case 84:
#line 345 "ldgram.y"
    { lang_add_map((yyvsp[-1].name)); }
    break;

  case 85:
#line 347 "ldgram.y"
    { ldlex_script (); ldfile_open_command_file((yyvsp[0].name)); }
    break;

  case 86:
#line 349 "ldgram.y"
    { ldlex_popstate (); }
    break;

  case 87:
#line 351 "ldgram.y"
    {
		  lang_add_nocrossref ((yyvsp[-1].nocrossref));
		}
    break;

  case 89:
#line 359 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_search_file_enum,
				 (char *)NULL); }
    break;

  case 90:
#line 362 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_search_file_enum,
				 (char *)NULL); }
    break;

  case 91:
#line 365 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_search_file_enum,
				 (char *)NULL); }
    break;

  case 92:
#line 368 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_l_enum,
				 (char *)NULL); }
    break;

  case 93:
#line 371 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_l_enum,
				 (char *)NULL); }
    break;

  case 94:
#line 374 "ldgram.y"
    { lang_add_input_file((yyvsp[0].name),lang_input_file_is_l_enum,
				 (char *)NULL); }
    break;

  case 95:
#line 377 "ldgram.y"
    { (yyval.integer) = as_needed; as_needed = TRUE; }
    break;

  case 96:
#line 379 "ldgram.y"
    { as_needed = (yyvsp[-2].integer); }
    break;

  case 97:
#line 381 "ldgram.y"
    { (yyval.integer) = as_needed; as_needed = TRUE; }
    break;

  case 98:
#line 383 "ldgram.y"
    { as_needed = (yyvsp[-2].integer); }
    break;

  case 99:
#line 385 "ldgram.y"
    { (yyval.integer) = as_needed; as_needed = TRUE; }
    break;

  case 100:
#line 387 "ldgram.y"
    { as_needed = (yyvsp[-2].integer); }
    break;

  case 105:
#line 402 "ldgram.y"
    { lang_add_entry ((yyvsp[-1].name), FALSE); }
    break;

  case 107:
#line 404 "ldgram.y"
    {ldlex_expression ();}
    break;

  case 108:
#line 405 "ldgram.y"
    { ldlex_popstate ();
		  lang_add_assignment (exp_assert ((yyvsp[-3].etree), (yyvsp[-1].name))); }
    break;

  case 109:
#line 413 "ldgram.y"
    {
			  (yyval.cname) = (yyvsp[0].name);
			}
    break;

  case 110:
#line 417 "ldgram.y"
    {
			  (yyval.cname) = "*";
			}
    break;

  case 111:
#line 421 "ldgram.y"
    {
			  (yyval.cname) = "?";
			}
    break;

  case 112:
#line 428 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[0].cname);
			  (yyval.wildcard).sorted = none;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 113:
#line 434 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[0].cname);
			  (yyval.wildcard).sorted = none;
			  (yyval.wildcard).exclude_name_list = (yyvsp[-2].name_list);
			}
    break;

  case 114:
#line 440 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-1].cname);
			  (yyval.wildcard).sorted = by_name;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 115:
#line 446 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-1].cname);
			  (yyval.wildcard).sorted = by_alignment;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 116:
#line 452 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-2].cname);
			  (yyval.wildcard).sorted = by_name_alignment;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 117:
#line 458 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-2].cname);
			  (yyval.wildcard).sorted = by_name;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 118:
#line 464 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-2].cname);
			  (yyval.wildcard).sorted = by_alignment_name;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 119:
#line 470 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-2].cname);
			  (yyval.wildcard).sorted = by_alignment;
			  (yyval.wildcard).exclude_name_list = NULL;
			}
    break;

  case 120:
#line 476 "ldgram.y"
    {
			  (yyval.wildcard).name = (yyvsp[-1].cname);
			  (yyval.wildcard).sorted = by_name;
			  (yyval.wildcard).exclude_name_list = (yyvsp[-3].name_list);
			}
    break;

  case 121:
#line 485 "ldgram.y"
    {
			  struct name_list *tmp;
			  tmp = (struct name_list *) xmalloc (sizeof *tmp);
			  tmp->name = (yyvsp[0].cname);
			  tmp->next = (yyvsp[-1].name_list);
			  (yyval.name_list) = tmp;
			}
    break;

  case 122:
#line 494 "ldgram.y"
    {
			  struct name_list *tmp;
			  tmp = (struct name_list *) xmalloc (sizeof *tmp);
			  tmp->name = (yyvsp[0].cname);
			  tmp->next = NULL;
			  (yyval.name_list) = tmp;
			}
    break;

  case 123:
#line 505 "ldgram.y"
    {
			  struct wildcard_list *tmp;
			  tmp = (struct wildcard_list *) xmalloc (sizeof *tmp);
			  tmp->next = (yyvsp[-2].wildcard_list);
			  tmp->spec = (yyvsp[0].wildcard);
			  (yyval.wildcard_list) = tmp;
			}
    break;

  case 124:
#line 514 "ldgram.y"
    {
			  struct wildcard_list *tmp;
			  tmp = (struct wildcard_list *) xmalloc (sizeof *tmp);
			  tmp->next = NULL;
			  tmp->spec = (yyvsp[0].wildcard);
			  (yyval.wildcard_list) = tmp;
			}
    break;

  case 125:
#line 525 "ldgram.y"
    {
			  struct wildcard_spec tmp;
			  tmp.name = (yyvsp[0].name);
			  tmp.exclude_name_list = NULL;
			  tmp.sorted = none;
			  lang_add_wild (&tmp, NULL, ldgram_had_keep);
			}
    break;

  case 126:
#line 533 "ldgram.y"
    {
			  lang_add_wild (NULL, (yyvsp[-1].wildcard_list), ldgram_had_keep);
			}
    break;

  case 127:
#line 537 "ldgram.y"
    {
			  lang_add_wild (&(yyvsp[-3].wildcard), (yyvsp[-1].wildcard_list), ldgram_had_keep);
			}
    break;

  case 129:
#line 545 "ldgram.y"
    { ldgram_had_keep = TRUE; }
    break;

  case 130:
#line 547 "ldgram.y"
    { ldgram_had_keep = FALSE; }
    break;

  case 132:
#line 553 "ldgram.y"
    {
 		lang_add_attribute(lang_object_symbols_statement_enum);
	      	}
    break;

  case 134:
#line 558 "ldgram.y"
    {

		  lang_add_attribute(lang_constructors_statement_enum);
		}
    break;

  case 135:
#line 563 "ldgram.y"
    {
		  constructors_sorted = TRUE;
		  lang_add_attribute (lang_constructors_statement_enum);
		}
    break;

  case 137:
#line 569 "ldgram.y"
    {
			  lang_add_data ((int) (yyvsp[-3].integer), (yyvsp[-1].etree));
			}
    break;

  case 138:
#line 574 "ldgram.y"
    {
			  lang_add_fill ((yyvsp[-1].fill));
			}
    break;

  case 139:
#line 577 "ldgram.y"
    {ldlex_expression ();}
    break;

  case 140:
#line 578 "ldgram.y"
    { ldlex_popstate ();
			  lang_add_assignment (exp_assert ((yyvsp[-4].etree), (yyvsp[-2].name))); }
    break;

  case 145:
#line 594 "ldgram.y"
    { (yyval.integer) = (yyvsp[0].token); }
    break;

  case 146:
#line 596 "ldgram.y"
    { (yyval.integer) = (yyvsp[0].token); }
    break;

  case 147:
#line 598 "ldgram.y"
    { (yyval.integer) = (yyvsp[0].token); }
    break;

  case 148:
#line 600 "ldgram.y"
    { (yyval.integer) = (yyvsp[0].token); }
    break;

  case 149:
#line 602 "ldgram.y"
    { (yyval.integer) = (yyvsp[0].token); }
    break;

  case 150:
#line 607 "ldgram.y"
    {
		  (yyval.fill) = exp_get_fill ((yyvsp[0].etree), 0, "fill value");
		}
    break;

  case 151:
#line 614 "ldgram.y"
    { (yyval.fill) = (yyvsp[0].fill); }
    break;

  case 152:
#line 615 "ldgram.y"
    { (yyval.fill) = (fill_type *) 0; }
    break;

  case 153:
#line 620 "ldgram.y"
    { (yyval.token) = '+'; }
    break;

  case 154:
#line 622 "ldgram.y"
    { (yyval.token) = '-'; }
    break;

  case 155:
#line 624 "ldgram.y"
    { (yyval.token) = '*'; }
    break;

  case 156:
#line 626 "ldgram.y"
    { (yyval.token) = '/'; }
    break;

  case 157:
#line 628 "ldgram.y"
    { (yyval.token) = LSHIFT; }
    break;

  case 158:
#line 630 "ldgram.y"
    { (yyval.token) = RSHIFT; }
    break;

  case 159:
#line 632 "ldgram.y"
    { (yyval.token) = '&'; }
    break;

  case 160:
#line 634 "ldgram.y"
    { (yyval.token) = '|'; }
    break;

  case 163:
#line 644 "ldgram.y"
    {
		  lang_add_assignment (exp_assop ((yyvsp[-1].token), (yyvsp[-2].name), (yyvsp[0].etree)));
		}
    break;

  case 164:
#line 648 "ldgram.y"
    {
		  lang_add_assignment (exp_assop ('=', (yyvsp[-2].name),
						  exp_binop ((yyvsp[-1].token),
							     exp_nameop (NAME,
									 (yyvsp[-2].name)),
							     (yyvsp[0].etree))));
		}
    break;

  case 165:
#line 656 "ldgram.y"
    {
		  lang_add_assignment (exp_provide ((yyvsp[-3].name), (yyvsp[-1].etree), FALSE));
		}
    break;

  case 166:
#line 660 "ldgram.y"
    {
		  lang_add_assignment (exp_provide ((yyvsp[-3].name), (yyvsp[-1].etree), TRUE));
		}
    break;

  case 173:
#line 682 "ldgram.y"
    { region = lang_memory_region_lookup ((yyvsp[0].name), TRUE); }
    break;

  case 174:
#line 685 "ldgram.y"
    {}
    break;

  case 175:
#line 690 "ldgram.y"
    {
		  region->origin = exp_get_vma ((yyvsp[0].etree), 0, "origin");
		  region->current = region->origin;
		}
    break;

  case 176:
#line 698 "ldgram.y"
    {
		  region->length = exp_get_vma ((yyvsp[0].etree), -1, "length");
		}
    break;

  case 177:
#line 705 "ldgram.y"
    { /* dummy action to avoid bison 1.25 error message */ }
    break;

  case 181:
#line 716 "ldgram.y"
    { lang_set_flags (region, (yyvsp[0].name), 0); }
    break;

  case 182:
#line 718 "ldgram.y"
    { lang_set_flags (region, (yyvsp[0].name), 1); }
    break;

  case 183:
#line 723 "ldgram.y"
    { lang_startup((yyvsp[-1].name)); }
    break;

  case 185:
#line 729 "ldgram.y"
    { ldemul_hll((char *)NULL); }
    break;

  case 186:
#line 734 "ldgram.y"
    { ldemul_hll((yyvsp[0].name)); }
    break;

  case 187:
#line 736 "ldgram.y"
    { ldemul_hll((yyvsp[0].name)); }
    break;

  case 189:
#line 744 "ldgram.y"
    { ldemul_syslib((yyvsp[0].name)); }
    break;

  case 191:
#line 750 "ldgram.y"
    { lang_float(TRUE); }
    break;

  case 192:
#line 752 "ldgram.y"
    { lang_float(FALSE); }
    break;

  case 193:
#line 757 "ldgram.y"
    {
		  (yyval.nocrossref) = NULL;
		}
    break;

  case 194:
#line 761 "ldgram.y"
    {
		  struct lang_nocrossref *n;

		  n = (struct lang_nocrossref *) xmalloc (sizeof *n);
		  n->name = (yyvsp[-1].name);
		  n->next = (yyvsp[0].nocrossref);
		  (yyval.nocrossref) = n;
		}
    break;

  case 195:
#line 770 "ldgram.y"
    {
		  struct lang_nocrossref *n;

		  n = (struct lang_nocrossref *) xmalloc (sizeof *n);
		  n->name = (yyvsp[-2].name);
		  n->next = (yyvsp[0].nocrossref);
		  (yyval.nocrossref) = n;
		}
    break;

  case 196:
#line 780 "ldgram.y"
    { ldlex_expression (); }
    break;

  case 197:
#line 782 "ldgram.y"
    { ldlex_popstate (); (yyval.etree)=(yyvsp[0].etree);}
    break;

  case 198:
#line 787 "ldgram.y"
    { (yyval.etree) = exp_unop ('-', (yyvsp[0].etree)); }
    break;

  case 199:
#line 789 "ldgram.y"
    { (yyval.etree) = (yyvsp[-1].etree); }
    break;

  case 200:
#line 791 "ldgram.y"
    { (yyval.etree) = exp_unop ((int) (yyvsp[-3].integer),(yyvsp[-1].etree)); }
    break;

  case 201:
#line 793 "ldgram.y"
    { (yyval.etree) = exp_unop ('!', (yyvsp[0].etree)); }
    break;

  case 202:
#line 795 "ldgram.y"
    { (yyval.etree) = (yyvsp[0].etree); }
    break;

  case 203:
#line 797 "ldgram.y"
    { (yyval.etree) = exp_unop ('~', (yyvsp[0].etree));}
    break;

  case 204:
#line 800 "ldgram.y"
    { (yyval.etree) = exp_binop ('*', (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 205:
#line 802 "ldgram.y"
    { (yyval.etree) = exp_binop ('/', (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 206:
#line 804 "ldgram.y"
    { (yyval.etree) = exp_binop ('%', (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 207:
#line 806 "ldgram.y"
    { (yyval.etree) = exp_binop ('+', (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 208:
#line 808 "ldgram.y"
    { (yyval.etree) = exp_binop ('-' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 209:
#line 810 "ldgram.y"
    { (yyval.etree) = exp_binop (LSHIFT , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 210:
#line 812 "ldgram.y"
    { (yyval.etree) = exp_binop (RSHIFT , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 211:
#line 814 "ldgram.y"
    { (yyval.etree) = exp_binop (EQ , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 212:
#line 816 "ldgram.y"
    { (yyval.etree) = exp_binop (NE , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 213:
#line 818 "ldgram.y"
    { (yyval.etree) = exp_binop (LE , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 214:
#line 820 "ldgram.y"
    { (yyval.etree) = exp_binop (GE , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 215:
#line 822 "ldgram.y"
    { (yyval.etree) = exp_binop ('<' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 216:
#line 824 "ldgram.y"
    { (yyval.etree) = exp_binop ('>' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 217:
#line 826 "ldgram.y"
    { (yyval.etree) = exp_binop ('&' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 218:
#line 828 "ldgram.y"
    { (yyval.etree) = exp_binop ('^' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 219:
#line 830 "ldgram.y"
    { (yyval.etree) = exp_binop ('|' , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 220:
#line 832 "ldgram.y"
    { (yyval.etree) = exp_trinop ('?' , (yyvsp[-4].etree), (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 221:
#line 834 "ldgram.y"
    { (yyval.etree) = exp_binop (ANDAND , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 222:
#line 836 "ldgram.y"
    { (yyval.etree) = exp_binop (OROR , (yyvsp[-2].etree), (yyvsp[0].etree)); }
    break;

  case 223:
#line 838 "ldgram.y"
    { (yyval.etree) = exp_nameop (DEFINED, (yyvsp[-1].name)); }
    break;

  case 224:
#line 840 "ldgram.y"
    { (yyval.etree) = exp_bigintop ((yyvsp[0].bigint).integer, (yyvsp[0].bigint).str); }
    break;

  case 225:
#line 842 "ldgram.y"
    { (yyval.etree) = exp_nameop (SIZEOF_HEADERS,0); }
    break;

  case 226:
#line 845 "ldgram.y"
    { (yyval.etree) = exp_nameop (ALIGNOF,(yyvsp[-1].name)); }
    break;

  case 227:
#line 847 "ldgram.y"
    { (yyval.etree) = exp_nameop (SIZEOF,(yyvsp[-1].name)); }
    break;

  case 228:
#line 849 "ldgram.y"
    { (yyval.etree) = exp_nameop (ADDR,(yyvsp[-1].name)); }
    break;

  case 229:
#line 851 "ldgram.y"
    { (yyval.etree) = exp_nameop (LOADADDR,(yyvsp[-1].name)); }
    break;

  case 230:
#line 853 "ldgram.y"
    { (yyval.etree) = exp_nameop (CONSTANT,(yyvsp[-1].name)); }
    break;

  case 231:
#line 855 "ldgram.y"
    { (yyval.etree) = exp_unop (ABSOLUTE, (yyvsp[-1].etree)); }
    break;

  case 232:
#line 857 "ldgram.y"
    { (yyval.etree) = exp_unop (ALIGN_K,(yyvsp[-1].etree)); }
    break;

  case 233:
#line 859 "ldgram.y"
    { (yyval.etree) = exp_binop (ALIGN_K,(yyvsp[-3].etree),(yyvsp[-1].etree)); }
    break;

  case 234:
#line 861 "ldgram.y"
    { (yyval.etree) = exp_binop (DATA_SEGMENT_ALIGN, (yyvsp[-3].etree), (yyvsp[-1].etree)); }
    break;

  case 235:
#line 863 "ldgram.y"
    { (yyval.etree) = exp_binop (DATA_SEGMENT_RELRO_END, (yyvsp[-1].etree), (yyvsp[-3].etree)); }
    break;

  case 236:
#line 865 "ldgram.y"
    { (yyval.etree) = exp_unop (DATA_SEGMENT_END, (yyvsp[-1].etree)); }
    break;

  case 237:
#line 867 "ldgram.y"
    { /* The operands to the expression node are
			     placed in the opposite order from the way
			     in which they appear in the script as
			     that allows us to reuse more code in
			     fold_binary.  */
			  (yyval.etree) = exp_binop (SEGMENT_START,
					  (yyvsp[-1].etree),
					  exp_nameop (NAME, (yyvsp[-3].name))); }
    break;

  case 238:
#line 876 "ldgram.y"
    { (yyval.etree) = exp_unop (ALIGN_K,(yyvsp[-1].etree)); }
    break;

  case 239:
#line 878 "ldgram.y"
    { (yyval.etree) = exp_nameop (NAME,(yyvsp[0].name)); }
    break;

  case 240:
#line 880 "ldgram.y"
    { (yyval.etree) = exp_binop (MAX_K, (yyvsp[-3].etree), (yyvsp[-1].etree) ); }
    break;

  case 241:
#line 882 "ldgram.y"
    { (yyval.etree) = exp_binop (MIN_K, (yyvsp[-3].etree), (yyvsp[-1].etree) ); }
    break;

  case 242:
#line 884 "ldgram.y"
    { (yyval.etree) = exp_unop (CEILP2, (yyvsp[-1].etree)); }
    break;

  case 243:
#line 886 "ldgram.y"
    { (yyval.etree) = exp_unop (NACL_MASK, (yyvsp[-1].etree)); }
    break;

  case 244:
#line 888 "ldgram.y"
    { (yyval.etree) = exp_assert ((yyvsp[-3].etree), (yyvsp[-1].name)); }
    break;

  case 245:
#line 890 "ldgram.y"
    { (yyval.etree) = exp_nameop (ORIGIN, (yyvsp[-1].name)); }
    break;

  case 246:
#line 892 "ldgram.y"
    { (yyval.etree) = exp_nameop (LENGTH, (yyvsp[-1].name)); }
    break;

  case 247:
#line 897 "ldgram.y"
    { (yyval.name) = (yyvsp[0].name); }
    break;

  case 248:
#line 898 "ldgram.y"
    { (yyval.name) = 0; }
    break;

  case 249:
#line 902 "ldgram.y"
    { (yyval.etree) = (yyvsp[-1].etree); }
    break;

  case 250:
#line 903 "ldgram.y"
    { (yyval.etree) = 0; }
    break;

  case 251:
#line 907 "ldgram.y"
    { (yyval.etree) = (yyvsp[-1].etree); }
    break;

  case 252:
#line 908 "ldgram.y"
    { (yyval.etree) = 0; }
    break;

  case 253:
#line 912 "ldgram.y"
    { (yyval.etree) = (yyvsp[-1].etree); }
    break;

  case 254:
#line 913 "ldgram.y"
    { (yyval.etree) = 0; }
    break;

  case 255:
#line 917 "ldgram.y"
    { (yyval.token) = ONLY_IF_RO; }
    break;

  case 256:
#line 918 "ldgram.y"
    { (yyval.token) = ONLY_IF_RW; }
    break;

  case 257:
#line 919 "ldgram.y"
    { (yyval.token) = SPECIAL; }
    break;

  case 258:
#line 920 "ldgram.y"
    { (yyval.token) = 0; }
    break;

  case 259:
#line 923 "ldgram.y"
    { ldlex_expression(); }
    break;

  case 260:
#line 927 "ldgram.y"
    { ldlex_popstate (); ldlex_script (); }
    break;

  case 261:
#line 930 "ldgram.y"
    {
			  lang_enter_output_section_statement((yyvsp[-8].name), (yyvsp[-6].etree),
							      sectype,
							      (yyvsp[-4].etree), (yyvsp[-3].etree), (yyvsp[-5].etree), (yyvsp[-1].token));
			}
    break;

  case 262:
#line 936 "ldgram.y"
    { ldlex_popstate (); ldlex_expression (); }
    break;

  case 263:
#line 938 "ldgram.y"
    {
		  ldlex_popstate ();
		  lang_leave_output_section_statement ((yyvsp[0].fill), (yyvsp[-3].name), (yyvsp[-1].section_phdr), (yyvsp[-2].name));
		}
    break;

  case 264:
#line 943 "ldgram.y"
    {}
    break;

  case 265:
#line 945 "ldgram.y"
    { ldlex_expression (); }
    break;

  case 266:
#line 947 "ldgram.y"
    { ldlex_popstate (); ldlex_script (); }
    break;

  case 267:
#line 949 "ldgram.y"
    {
			  lang_enter_overlay ((yyvsp[-5].etree), (yyvsp[-2].etree));
			}
    break;

  case 268:
#line 954 "ldgram.y"
    { ldlex_popstate (); ldlex_expression (); }
    break;

  case 269:
#line 956 "ldgram.y"
    {
			  ldlex_popstate ();
			  lang_leave_overlay ((yyvsp[-11].etree), (int) (yyvsp[-12].integer),
					      (yyvsp[0].fill), (yyvsp[-3].name), (yyvsp[-1].section_phdr), (yyvsp[-2].name));
			}
    break;

  case 271:
#line 966 "ldgram.y"
    { ldlex_expression (); }
    break;

  case 272:
#line 968 "ldgram.y"
    {
		  ldlex_popstate ();
		  lang_add_assignment (exp_assop ('=', ".", (yyvsp[0].etree)));
		}
    break;

  case 274:
#line 976 "ldgram.y"
    { sectype = noload_section; }
    break;

  case 275:
#line 977 "ldgram.y"
    { sectype = noalloc_section; }
    break;

  case 276:
#line 978 "ldgram.y"
    { sectype = noalloc_section; }
    break;

  case 277:
#line 979 "ldgram.y"
    { sectype = noalloc_section; }
    break;

  case 278:
#line 980 "ldgram.y"
    { sectype = noalloc_section; }
    break;

  case 280:
#line 985 "ldgram.y"
    { sectype = normal_section; }
    break;

  case 281:
#line 986 "ldgram.y"
    { sectype = normal_section; }
    break;

  case 282:
#line 990 "ldgram.y"
    { (yyval.etree) = (yyvsp[-2].etree); }
    break;

  case 283:
#line 991 "ldgram.y"
    { (yyval.etree) = (etree_type *)NULL;  }
    break;

  case 284:
#line 996 "ldgram.y"
    { (yyval.etree) = (yyvsp[-3].etree); }
    break;

  case 285:
#line 998 "ldgram.y"
    { (yyval.etree) = (yyvsp[-7].etree); }
    break;

  case 286:
#line 1002 "ldgram.y"
    { (yyval.etree) = (yyvsp[-1].etree); }
    break;

  case 287:
#line 1003 "ldgram.y"
    { (yyval.etree) = (etree_type *) NULL;  }
    break;

  case 288:
#line 1008 "ldgram.y"
    { (yyval.integer) = 0; }
    break;

  case 289:
#line 1010 "ldgram.y"
    { (yyval.integer) = 1; }
    break;

  case 290:
#line 1015 "ldgram.y"
    { (yyval.name) = (yyvsp[0].name); }
    break;

  case 291:
#line 1016 "ldgram.y"
    { (yyval.name) = DEFAULT_MEMORY_REGION; }
    break;

  case 292:
#line 1021 "ldgram.y"
    {
		  (yyval.section_phdr) = NULL;
		}
    break;

  case 293:
#line 1025 "ldgram.y"
    {
		  struct lang_output_section_phdr_list *n;

		  n = ((struct lang_output_section_phdr_list *)
		       xmalloc (sizeof *n));
		  n->name = (yyvsp[0].name);
		  n->used = FALSE;
		  n->next = (yyvsp[-2].section_phdr);
		  (yyval.section_phdr) = n;
		}
    break;

  case 295:
#line 1041 "ldgram.y"
    {
			  ldlex_script ();
			  lang_enter_overlay_section ((yyvsp[0].name));
			}
    break;

  case 296:
#line 1046 "ldgram.y"
    { ldlex_popstate (); ldlex_expression (); }
    break;

  case 297:
#line 1048 "ldgram.y"
    {
			  ldlex_popstate ();
			  lang_leave_overlay_section ((yyvsp[0].fill), (yyvsp[-1].section_phdr));
			}
    break;

  case 302:
#line 1065 "ldgram.y"
    { ldlex_expression (); }
    break;

  case 303:
#line 1066 "ldgram.y"
    { ldlex_popstate (); }
    break;

  case 304:
#line 1068 "ldgram.y"
    {
		  lang_new_phdr ((yyvsp[-5].name), (yyvsp[-3].etree), (yyvsp[-2].phdr).filehdr, (yyvsp[-2].phdr).phdrs, (yyvsp[-2].phdr).at,
				 (yyvsp[-2].phdr).flags);
		}
    break;

  case 305:
#line 1076 "ldgram.y"
    {
		  (yyval.etree) = (yyvsp[0].etree);

		  if ((yyvsp[0].etree)->type.node_class == etree_name
		      && (yyvsp[0].etree)->type.node_code == NAME)
		    {
		      const char *s;
		      unsigned int i;
		      static const char * const phdr_types[] =
			{
			  "PT_NULL", "PT_LOAD", "PT_DYNAMIC",
			  "PT_INTERP", "PT_NOTE", "PT_SHLIB",
			  "PT_PHDR", "PT_TLS"
			};

		      s = (yyvsp[0].etree)->name.name;
		      for (i = 0;
			   i < sizeof phdr_types / sizeof phdr_types[0];
			   i++)
			if (strcmp (s, phdr_types[i]) == 0)
			  {
			    (yyval.etree) = exp_intop (i);
			    break;
			  }
		      if (i == sizeof phdr_types / sizeof phdr_types[0])
			{
			  if (strcmp (s, "PT_GNU_EH_FRAME") == 0)
			    (yyval.etree) = exp_intop (0x6474e550);
			  else if (strcmp (s, "PT_GNU_STACK") == 0)
			    (yyval.etree) = exp_intop (0x6474e551);
			  else
			    {
			      einfo (_("\
%X%P:%S: unknown phdr type `%s' (try integer literal)\n"),
				     s);
			      (yyval.etree) = exp_intop (0);
			    }
			}
		    }
		}
    break;

  case 306:
#line 1120 "ldgram.y"
    {
		  memset (&(yyval.phdr), 0, sizeof (struct phdr_info));
		}
    break;

  case 307:
#line 1124 "ldgram.y"
    {
		  (yyval.phdr) = (yyvsp[0].phdr);
		  if (strcmp ((yyvsp[-2].name), "FILEHDR") == 0 && (yyvsp[-1].etree) == NULL)
		    (yyval.phdr).filehdr = TRUE;
		  else if (strcmp ((yyvsp[-2].name), "PHDRS") == 0 && (yyvsp[-1].etree) == NULL)
		    (yyval.phdr).phdrs = TRUE;
		  else if (strcmp ((yyvsp[-2].name), "FLAGS") == 0 && (yyvsp[-1].etree) != NULL)
		    (yyval.phdr).flags = (yyvsp[-1].etree);
		  else
		    einfo (_("%X%P:%S: PHDRS syntax error at `%s'\n"), (yyvsp[-2].name));
		}
    break;

  case 308:
#line 1136 "ldgram.y"
    {
		  (yyval.phdr) = (yyvsp[0].phdr);
		  (yyval.phdr).at = (yyvsp[-2].etree);
		}
    break;

  case 309:
#line 1144 "ldgram.y"
    {
		  (yyval.etree) = NULL;
		}
    break;

  case 310:
#line 1148 "ldgram.y"
    {
		  (yyval.etree) = (yyvsp[-1].etree);
		}
    break;

  case 311:
#line 1154 "ldgram.y"
    {
		  ldlex_version_file ();
		  PUSH_ERROR (_("dynamic list"));
		}
    break;

  case 312:
#line 1159 "ldgram.y"
    {
		  ldlex_popstate ();
		  POP_ERROR ();
		}
    break;

  case 316:
#line 1176 "ldgram.y"
    {
		  lang_append_dynamic_list ((yyvsp[-1].versyms));
		}
    break;

  case 317:
#line 1184 "ldgram.y"
    {
		  ldlex_version_file ();
		  PUSH_ERROR (_("VERSION script"));
		}
    break;

  case 318:
#line 1189 "ldgram.y"
    {
		  ldlex_popstate ();
		  POP_ERROR ();
		}
    break;

  case 319:
#line 1198 "ldgram.y"
    {
		  ldlex_version_script ();
		}
    break;

  case 320:
#line 1202 "ldgram.y"
    {
		  ldlex_popstate ();
		}
    break;

  case 323:
#line 1214 "ldgram.y"
    {
		  lang_register_vers_node (NULL, (yyvsp[-2].versnode), NULL);
		}
    break;

  case 324:
#line 1218 "ldgram.y"
    {
		  lang_register_vers_node ((yyvsp[-4].name), (yyvsp[-2].versnode), NULL);
		}
    break;

  case 325:
#line 1222 "ldgram.y"
    {
		  lang_register_vers_node ((yyvsp[-5].name), (yyvsp[-3].versnode), (yyvsp[-1].deflist));
		}
    break;

  case 326:
#line 1229 "ldgram.y"
    {
		  (yyval.deflist) = lang_add_vers_depend (NULL, (yyvsp[0].name));
		}
    break;

  case 327:
#line 1233 "ldgram.y"
    {
		  (yyval.deflist) = lang_add_vers_depend ((yyvsp[-1].deflist), (yyvsp[0].name));
		}
    break;

  case 328:
#line 1240 "ldgram.y"
    {
		  (yyval.versnode) = lang_new_vers_node (NULL, NULL);
		}
    break;

  case 329:
#line 1244 "ldgram.y"
    {
		  (yyval.versnode) = lang_new_vers_node ((yyvsp[-1].versyms), NULL);
		}
    break;

  case 330:
#line 1248 "ldgram.y"
    {
		  (yyval.versnode) = lang_new_vers_node ((yyvsp[-1].versyms), NULL);
		}
    break;

  case 331:
#line 1252 "ldgram.y"
    {
		  (yyval.versnode) = lang_new_vers_node (NULL, (yyvsp[-1].versyms));
		}
    break;

  case 332:
#line 1256 "ldgram.y"
    {
		  (yyval.versnode) = lang_new_vers_node ((yyvsp[-5].versyms), (yyvsp[-1].versyms));
		}
    break;

  case 333:
#line 1263 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern (NULL, (yyvsp[0].name), ldgram_vers_current_lang, FALSE);
		}
    break;

  case 334:
#line 1267 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern (NULL, (yyvsp[0].name), ldgram_vers_current_lang, TRUE);
		}
    break;

  case 335:
#line 1271 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern ((yyvsp[-2].versyms), (yyvsp[0].name), ldgram_vers_current_lang, FALSE);
		}
    break;

  case 336:
#line 1275 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern ((yyvsp[-2].versyms), (yyvsp[0].name), ldgram_vers_current_lang, TRUE);
		}
    break;

  case 337:
#line 1279 "ldgram.y"
    {
			  (yyval.name) = ldgram_vers_current_lang;
			  ldgram_vers_current_lang = (yyvsp[-1].name);
			}
    break;

  case 338:
#line 1284 "ldgram.y"
    {
			  struct bfd_elf_version_expr *pat;
			  for (pat = (yyvsp[-2].versyms); pat->next != NULL; pat = pat->next);
			  pat->next = (yyvsp[-8].versyms);
			  (yyval.versyms) = (yyvsp[-2].versyms);
			  ldgram_vers_current_lang = (yyvsp[-3].name);
			}
    break;

  case 339:
#line 1292 "ldgram.y"
    {
			  (yyval.name) = ldgram_vers_current_lang;
			  ldgram_vers_current_lang = (yyvsp[-1].name);
			}
    break;

  case 340:
#line 1297 "ldgram.y"
    {
			  (yyval.versyms) = (yyvsp[-2].versyms);
			  ldgram_vers_current_lang = (yyvsp[-3].name);
			}
    break;

  case 341:
#line 1302 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern (NULL, "global", ldgram_vers_current_lang, FALSE);
		}
    break;

  case 342:
#line 1306 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern ((yyvsp[-2].versyms), "global", ldgram_vers_current_lang, FALSE);
		}
    break;

  case 343:
#line 1310 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern (NULL, "local", ldgram_vers_current_lang, FALSE);
		}
    break;

  case 344:
#line 1314 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern ((yyvsp[-2].versyms), "local", ldgram_vers_current_lang, FALSE);
		}
    break;

  case 345:
#line 1318 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern (NULL, "extern", ldgram_vers_current_lang, FALSE);
		}
    break;

  case 346:
#line 1322 "ldgram.y"
    {
		  (yyval.versyms) = lang_new_vers_pattern ((yyvsp[-2].versyms), "extern", ldgram_vers_current_lang, FALSE);
		}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 3991 "ldgram.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping", yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 1332 "ldgram.y"

void
yyerror(arg)
     const char *arg;
{
  if (ldfile_assumed_script)
    einfo (_("%P:%s: file format not recognized; treating as linker script\n"),
	   ldfile_input_filename);
  if (error_index > 0 && error_index < ERROR_NAME_MAX)
     einfo ("%P%F:%S: %s in %s\n", arg, error_names[error_index-1]);
  else
     einfo ("%P%F:%S: %s\n", arg);
}

